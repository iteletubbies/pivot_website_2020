@extends("$frontend")

@section('title-agent')
    {{-- {{ $title_agent }} --}}
    {{ config('app.name')}}
@endsection

@section('style-header')
    <style>

        @media (max-width: 768px) {

            .career-header{
                padding: 60px 30px 0 30px ;
            }

            .career-img-bg{
                background-image: url({{ asset("img/career/cover.png") }});

                /* Center and scale the image nicely */
                width: 100%;
                height: 487px;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            .about-h1{
                font-size: 36px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
            }

            .about-p{
                font-size: 26px;
                line-height: 34px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                /* padding: 0 15px ; */
            }

            .goal {
                background-color: #F5F5F5;
            }

            .about-goal-h1{
                font-size: 36px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;
            }

            .about-goal-p{
                font-family: 'cschatthaiUI' ;

                font-size: 26px;
                line-height: 36x;
            }

            .about-goal-p-description {
                color: #191919;
                font-size: 20px;
                line-height: 26px;

                margin-top: -16px;
                padding-bottom: 15px;

            }

            .about-story-p-description {
                font-size: 20px;
                line-height: 25px;
                font-weight: 100;
            }

            .job{
                background-color: #fff;
            }

            .btn-collapse{

                position: relative;
                left: 0px;
                top: 0px;
                /* cursor: pointer; */
                /* padding: 18px; */
                width: 100%;
                height: 50px;
                border: 0px;
                font-size: 20px;
                line-height: 34px;
                background-color: #fff;

                /* border-bottom: 3px solid red; */
            }

            /* .btn-collapse:hover {
                background-color: #ccc;
            } */

            .btn-collapse:after {
                content: '\002B';
                font-weight: bold;
                position: relative;
                top: -60px;
                left: 150px;

            }

            .actived:after {
                content: "\2212";
            }

            .btn-collapse-other{
                width: 100%;
                height: 50px;
                border: 0px;
                font-size: 26px;
                line-height: 34px;
                background-color: #fff;
            }

            .btn-collapse-other:after {
                content: '\002B';
                font-weight: bold;

                float: right;
                /* margin-left: 5px; */

            }

            .job-content {
                max-height: 0;
                overflow: hidden;
                transition: max-height 0.5s ease-out;
                background-color: #fff;

                font-size: 20px;
                line-height: 26px;

                /* padding-left: 5vh; */
            }

            .btn-primary{
                background-color: #FB872D;
                border-radius: 3px;
                border: 0px;
                font-size: 18px;
            }

            hr {
                border-top: 1px solid black;
            }

            .register-p-1{
                font-family: 'cschatthai' ;
                font-size: 26px;
                line-height: 35.79px;
            }

            .bg-career{
                background-color: #fff;
            }

            .title-ul{
                font-family: 'cschatthai' ;
                font-size: 20px;
                line-height: 35.79px;
            }

            .btn-register{
                font-family: 'cschatthai' ;
                font-size: 20px;
                color:#FFFFFF;
                background-color:#FB872D;
                border-radius: 3px;
                width:109px;

            }
            .btn-register:hover{
                color:#191919;
            }
            .btn-register:visited{
                color:#FFFFFF;
                background-color:#FB872D;
            }

            .btn-link {
                font-weight: 400;
                color: #FFA73B;
                text-decoration: none;
            }

            .btn-link:hover {
                color: #FFA73B;
            }
        }

        @if( $title_agent == 'isDesktop')

            section {
                padding:0;

            }

            .career-header{
                padding: 12vw 0 0 0;
            }

            .career-img-bg{
                background-image: url({{ asset("img/career/big-cover.jpg") }});
                width: 100%;
                height: 645px;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            .about-h1{
                font-size: 64px;
                font-family: 'cschatthaiUI' ;
                line-height: 88.1px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
            }

            .about-p{
                font-size: 40px;
                line-height: 51.8px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                /* padding: 0 15px ; */
            }

            .goal {
                background-color: #F5F5F5;
            }

            .about-goal-h1{
                font-size: 36px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;
            }

            .about-goal-p{
                font-family: 'cschatthaiUI' ;

                font-size: 26px;
                line-height: 35.79px;
            }

            .about-goal-p-description {
                color: #1E1E1E;
                font-size: 20px;
                line-height: 25.9px;
            }

            .about-story-p-description {
                font-size: 20px;
                line-height: 25px;
                font-weight: 100;
            }

            .img-fluid{
                height: 60px;
            }

            .job{
                background-color: #fff;
            }

            .btn-collapse{

                position: relative;
                left: 0px;
                top: 0px;
                /* cursor: pointer; */
                /* padding: 18px; */
                width: 100%;
                height: 70px;
                border: 0px;
                font-size: 26px;
                line-height: 34px;
                background-color: #fff;

                /* border-bottom: 3px solid red; */
            }

            /* .btn-collapse:hover {
                background-color: #ccc;
            } */

            .btn-collapse:after {
                content: '\002B';
                font-weight: bold;

                /* float: right;
                margin-left: 5px; */
                position: relative;
                top: -60px;
                left: 150px;

            }

            .actived:after {
                content: "\2212";
            }

            .btn-collapse-other{
                width: 100%;
                height: 50px;
                border: 0px;
                font-size: 26px;
                line-height: 34px;
                background-color: #fff;
            }

            .btn-collapse-other:after {
                content: '\002B';
                font-weight: bold;

                float: right;
                /* margin-left: 5px; */

            }

            .job-content {
                max-height: 0;
                overflow: hidden;
                transition: max-height 0.5s ease-out;
                background-color: #fff;

                font-size: 20px;
                line-height: 26px;

                /* padding-left: 5vh; */
            }

            .btn-primary{
                background-color: #FB872D;
                border-radius: 3px;
                border: 0px;
                font-size: 18px;
            }

            hr {
                border-top: 1px solid black;
            }

            .register-p-1{
                font-family: 'cschatthai' ;
                font-size: 26px;
                line-height: 35.79px;
            }

            .btn-register{
                font-family: 'cschatthai' ;
                font-size: 20px;
                color:#FFFFFF;
                background-color:#FB872D;
                border-radius: 3px;
                width:109px;

            }
            .btn-register:hover{
                color:#191919;
            }
            .btn-register:visited{
                color:#FFFFFF;
                background-color:#FB872D;
            }

            .btn-link {
                font-weight: 400;
                color: #FFA73B;
                text-decoration: none;
            }

            .btn-link:hover {
                color: #FFA73B;
            }

            .bg-career{
                background-color: #fff;
            }

            .title-ul{
                font-family: 'cschatthai' ;
                font-size: 26px;
                line-height: 35.79px;
            }

        @endif



    </style>

@endsection

@section('footer-script')

    <script>
        var coll = document.getElementsByClassName("btn-collapse");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("actived");
                var content = this.nextElementSibling;
                if (content.style.maxHeight){
                content.style.maxHeight = null;
                } else {
                content.style.maxHeight = content.scrollHeight + "px";
                }
            });
        }

        var collapse = document.getElementsByClassName("btn-collapse-other");
        var a;

        for (i = 0; a < coll.length; a++) {
            coll[a].addEventListener("click", function() {
                this.classList.toggle("actived");
                var content = this.nextElementSibling;
                if (content.style.maxHeight){
                content.style.maxHeight = null;
                } else {
                content.style.maxHeight = content.scrollHeight + "px";
                }
            });
        }
    </script>

    <script>

        $(document).ready(function() {

            $('#collapseOne').on('show.bs.collapse', function () {

            $(".collapseOne").addClass('fas fa-minus').removeClass('fas fa-plus');
            })

            $('#collapseOne').on('hide.bs.collapse', function () {
            console.log('Suwit');
            $(".collapseOne").addClass('fas fa-plus').removeClass('fas fa-minus');
            })

            $('#collapseTwo').on('show.bs.collapse', function () {

            $(".collapseTwo").addClass('fas fa-minus').removeClass('fas fa-plus');
            })

            $('#collapseTwo').on('hide.bs.collapse', function () {

            $(".collapseTwo").addClass('fas fa-plus').removeClass('fas fa-minus');
            })


            $('#collapseTree').on('show.bs.collapse', function () {

            $(".collapseTree").addClass('fas fa-minus').removeClass('fas fa-plus');
            })

            $('#collapseTree').on('hide.bs.collapse', function () {

            $(".collapseTree").addClass('fas fa-plus').removeClass('fas fa-minus');
            })

            $('#collapseFour').on('show.bs.collapse', function () {

            $(".collapseFour").addClass('fas fa-minus').removeClass('fas fa-plus');
            })

            $('#collapseFour').on('hide.bs.collapse', function () {

            $(".collapseFour").addClass('fas fa-plus').removeClass('fas fa-minus');
            })

            $('#collapseFive').on('show.bs.collapse', function () {

            $(".collapseFive").addClass('fas fa-minus').removeClass('fas fa-plus');
            })

            $('#collapseFive').on('hide.bs.collapse', function () {

            $(".collapseFive").addClass('fas fa-plus').removeClass('fas fa-minus');
            })
        });

    </script>

@endsection

@section('content')

    <section id="career" class="career section-bg">
        <div class="container-fluid px-0">
            @if( $title_agent == 'isDesktop')
                <div class="career-img-bg">
                    <div class="container">
                        <div class="career-header text-white">
                            <h3 class="about-h1" data-aos="fade-up" >ก้าวไปข้างหน้าด้วยกัน</h3>
                            <p class="about-p" data-aos="fade-up" >เราสร้างคนไปพร้อมกับการสร้างแบรนด์
                                เป็นภารกิจสำคัญขององค์กร</p>
                        </div>
                    </div>

                </div>
            @endif
            @if( $title_agent == 'isMobile')
                <div class="career-header career-img-bg text-white">
                    <h3 class="about-h1 mt-4">ก้าวไปข้างหน้าด้วยกัน</h3>
                    <p class="about-p px-0">เราสร้างคนไปพร้อมกับการสร้างแบรนด์
                        เป็นภารกิจสำคัญขององค์กร</p>
                </div>
            @endif
        </div>
    </section>

    <main id="main">
        @if( $title_agent == 'isDesktop')
            <div class="goal pb-5">
                <div class="container">
                    <div data-aos="fade-up">
                        <h3 class="about-goal-h1 text-center py-5">3 สิ่งที่ไพวอทให้คุณได้</h3>
                    </div>

                    <div class="row text-center">
                        <div class="col-4" data-aos="fade-up">
                            <img src="{{ asset('img/career/icon-1.svg') }}" class="img-fluid">
                            <p class="about-goal-p mt-2">Good Teamwork</p>
                            <p class="about-goal-p-description">เน้นการทำงานเป็นทีมมีหลักสูตรฝึกอบรม<br>ที่เป็นมาตรฐานสนับสนุนพนักงานให้พัฒนา<br>ศักยภาพของตนเองอยู่เสมอ</p>
                        </div>

                        <div class="col-4" data-aos="fade-up">
                            <img src="{{ asset('img/career/icon-2.svg') }}" class="img-fluid">
                            <p class="about-goal-p mt-2">Good Benefit</p>
                            <p class="about-goal-p-description">ผลตอบแทนคุ้มค่า หมดห่วงเรื่องอุบัติเหตุ<br>เพราะเรามีประกันอุบัติเหตุคุ้มครอง<br>พร้อมด้วยสวัสดิการอื่นๆ อีกมากมาย</p>
                        </div>

                            <div class="col-4" data-aos="fade-up">
                                <img src="{{ asset('img/career/icon-3.svg') }}" class="img-fluid">
                                <p class="about-goal-p mt-2">Good Life</p>
                                <p class="about-goal-p-description">สร้างคนไปพร้อมกับการสร้างองค์กรความสำเร็จของเรามา<br>จากการมองความมั่นคงและความสุขของคนทำงาน<br>เป็นอันดับแรกและองค์กรเป็นลำดับถัดมา</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="bg-career pb-5">
                <div class="container">

                    <div>
                        <h3 class="about-goal-h1 pt-5">สมัครร่วมทีม</h3>
                        <hr />
                    </div>

                    <div class="row">
                            <div class="col-10 col-md-11">
                                <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาทองหล่อ</p>
                            </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                    <i class="collapseOne fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseOne" class="collapse " data-parent="#accordionExample">
                        <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank" " class="btn btn-register ">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาบางนา</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseTwo"
                                    aria-expanded="true" aria-controls="collapseTwo"></i>
                                    <i class="collapseTwo fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseTwo" class="collapse " data-parent="#accordionExample">
                        <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank"  class="btn btn-register ">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาเมืองทอง</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseTree"
                                    aria-expanded="true" aria-controls="collapseTree"></i>
                                    <i class="collapseTree fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseTree" class="collapse " data-parent="#accordionExample">
                        <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank"  class="btn btn-register">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาสมุทรสาคร</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseFour"
                                    aria-expanded="true" aria-controls="collapseFour"></i>
                                    <i class="collapseFour fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseFour" class="collapse " data-parent="#accordionExample">
                    <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank" " class="btn btn-register ">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานตำแหน่งอื่นๆ</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseFive"
                                    aria-expanded="true" aria-controls="collapseFive"></i>
                                    <i class="collapseFive fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseFive" class="collapse " data-parent="#accordionExample">
                    <div class="card w-100 border-0">
                            <div class="text-center">
                                <a href="https://www.jobbkk.com/jobs/profile/162876" target="_blank" class="btn btn-register">สมัครงาน</a>
                            </div>
                        </div>
                    </div>

                    <hr>
                </div>
            </div>

        @endif

        @if( $title_agent == 'isMobile')

            <section id="goal" class="goal">
                <div class="container">

                    <div data-aos="fade-up">
                        <h3 class="about-goal-h1 pl-3">3 สิ่งที่ไพวอทให้คุณได้</h3>
                    </div>

                    <div class="row pl-3">

                        <div class="col-xl d-flex align-items-stretch pt-4 pt-xl-0" >
                        <div class="content d-flex flex-column justify-content-center">
                            <div class="row">

                            <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                                <img src="{{ asset('img/career/icon-1.svg') }}" class="img-fluid">
                                <p class="about-goal-p mt-2">Good Teamwork</p>
                                <p class="about-goal-p-description">เน้นการทำงานเป็นทีมมีหลักสูตรฝึกอบรม
                                    ที่เป็นมาตรฐาน<br />สนับสนุนพนักงานให้พัฒนา<br />
                                    ศักยภาพของตนเองอยู่เสมอ</p>
                            </div>

                            <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                                <img src="{{ asset('img/career/icon-2.svg') }}" class="img-fluid">
                                <p class="about-goal-p mt-2">Good Benefit</p>
                                <p class="about-goal-p-description">ผลตอบแทนคุ้มค่า หมดห่วงเรื่องอุบัติเหตุ<br>
                                    เพราะเรามีประกันอุบัติเหตุคุ้มครอง<br>
                                    พร้อมด้วยสวัสดิการอื่นๆ อีกมากมาย</p>
                            </div>

                                <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                                    <img src="{{ asset('img/career/icon-3.svg') }}" class="img-fluid">
                                    <p class="about-goal-p mt-2">Good Life</p>
                                    <p class="about-goal-p-description">สร้างคนไปพร้อมกับการสร้างองค์กรความสำเร็จ
                                        ของเรามากจาก<br>การมองความมั่นคงและความสุขของ
                                        คนทำงานเป็นอันดับแรก<br>และองค์กรเป็นลำดับถัดมา</p>
                                </div>

                            </div>
                        </div><!-- End .content-->
                        </div>
                    </div>
                </div>
            </section>

            <div class="bg-career pb-5">
                <div class="container">

                    <div>
                        <h3 class="about-goal-h1 pt-5">สมัครร่วมทีม</h3>
                        <hr />
                    </div>

                    <div class="row">
                            <div class="col-10 col-md-11">
                                <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาทองหล่อ</p>
                            </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                    <i class="collapseOne fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseOne" class="collapse " data-parent="#accordionExample">
                        <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank" " class="btn btn-register ">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาบางนา</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseTwo"
                                    aria-expanded="true" aria-controls="collapseTwo"></i>
                                    <i class="collapseTwo fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseTwo" class="collapse " data-parent="#accordionExample">
                        <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank"  class="btn btn-register ">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาเมืองทอง</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseTree"
                                    aria-expanded="true" aria-controls="collapseTree"></i>
                                    <i class="collapseTree fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseTree" class="collapse " data-parent="#accordionExample">
                        <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank"  class="btn btn-register">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานแมสเซ็นเจอร์<br><i class="fas fa-map-marker-alt text-danger"></i>&nbsp&nbspสาขาสมุทรสาคร</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseFour"
                                    aria-expanded="true" aria-controls="collapseFour"></i>
                                    <i class="collapseFour fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseFour" class="collapse " data-parent="#accordionExample">
                    <div class="card w-100 border-0">

                                <div class="title-ul p-0">รายละเอียดงาน
                                    <div class="p-li">
                                        <ul>
                                            <li>รูปแบบงาน : งานประจำ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">หน้าที่ความรับผิดชอบ
                                    <div class="p-li">
                                        <ul>
                                            <li>ตรวจสอบกิจการ ตรวจสอบรายได้ – ต้นทุนของกิจการ ตรวจสอบความเป็นเจ้าของกิจการ เพื่อนำมาประกอบการพิจารณาสินเชื่อ</li>
                                            <li>รับเอกสารสำคัญของลูกค้า บัตรเครดิต และลูกค้าสินเชื่อ ของธนาคาร</li>
                                            <li>ปฏิบัติงานในเขตพื้นที่</li>
                                            <li>อื่น ๆ ที่ได้รับมอบหมาย</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">คุณสมบัติ
                                    <div class="p-li">
                                        <ul>
                                            <li>เพศ :ชาย/หญิง</li>
                                            <li>อายุไม่เกิน 45 ปี</li>
                                            <li>วุฒิการศึกษา ระดับ ปวส ขึ้นไป (ปริญญาตรีจะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>มีรถจักรยานยนต์ และมีใบอนุญาตขับขี่รถจักรยานยนต์เป็นของตัวเอง</li>
                                            <li>มีความรู้และประสบการณ์ด้านการตรวจสอบข้อมูลภาคสนาม (จะได้รับการพิจารณาเป็นพิเศษ)</li>
                                            <li>สามารถแก้ปัญหา ตัดสินใจ และปฏิบัติงานภายใต้แรงกดดัน และภายในเวลาที่กำหนด</li>
                                            <li>มีความละเอียดรอบคอบ และความกระตือรือร้น ซื่อสัตย์</li>
                                            <li>สามารถเริ่มปฏิบัติงานได้ทันที</li>
                                            <li>อื่นๆ : ยินดีรับนักศึกษาจบใหม่</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="title-ul p-0">สวัสดิการ
                                    <div class="p-li">
                                        <ul>
                                            <li>ประกันชีวิต</li>
                                            <li>ประกันสังคม</li>
                                            <li>ค่าตอบแทนพิเศษ</li>
                                            <li>ทำงานสัปดาห์ละ 5 วัน</li>
                                            <li>ค่าทำงานล่วงเวลา</li>
                                            <li>เงินโบนัสตามผลงาน</li>
                                            <li>ชุดยูนิฟอร์ม</li>
                                            <li>เบี้ยขยัน</li>
                                            <li>ท่องเที่ยวประจำปี</li>
                                            <li>ประกันอุบัติเหตุ</li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="text-center">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSeEsA6OisWuG-4DPkdSFK90I2AYde5IJbcSNXv9rNx7Qv_XzQ/viewform" target="_blank" " class="btn btn-register ">สมัครงาน</a>
                                </div>
                            </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-10 col-md-11">
                            <p class="m-0 register-p-1">สมัครร่วมงานตำแหน่งอื่นๆ</p>
                        </div>
                        <div class="col-2 col-md-1">
                            <div class="accordion bg-white pt-2" id="accordionExample">
                                <button class="btn btn-link text-right" type="button" data-toggle="collapse" data-target="#collapseFive"
                                    aria-expanded="true" aria-controls="collapseFive"></i>
                                    <i class="collapseFive fas fa-plus"></i>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div id="collapseFive" class="collapse " data-parent="#accordionExample">
                    <div class="card w-100 border-0">
                            <div class="text-center">
                                <a href="https://www.jobbkk.com/jobs/profile/162876" target="_blank" class="btn btn-register">สมัครงาน</a>
                            </div>
                        </div>
                    </div>

                    <hr>
                </div>
            </div>

        @endif

@endsection
