@extends("$frontend")

@section('title-agent')
    {{-- {{ $title_agent }} --}}
    {{ config('app.name')}}
@endsection

@section('style-header')
    <style>

        @media (max-width: 768px) {

            .about-header{
                padding: 120px 30px 0 30px ;
            }

            .about-img-bg{
                background-image: url({{ asset('img/about/cover.png') }});

                /* Center and scale the image nicely */
                width: 100%;
                height: 488px;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            .about-h1{
                font-size: 36px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
            }

            .about-p{
                font-size: 26px;
                line-height: 34px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                /* padding: 0 15px ; */
            }

            .about-target-title{
                font-family: 'cschatthaiUI' ;
                font-size: 36px;
                line-height: 50px;

                margin-top: 25px;
                margin-left: 15px;

            }

            .about-target-li {
                color: #191919;
                font-size: 20px;
                line-height: 26px;
                margin-left: 15px;
                padding-bottom: 30px;
            }

            .slogan-mission-img{

                margin-top: -30px;
                box-shadow:0 10px 16px 0 rgba(0,0,0,0.25),0 6px 20px 0 rgba(0,0,0,0.19) !important;
                border-radius: 10px;

                /* drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.25)); */

            }

            .goal {
                background-color: #F5F5F5;
            }

            .about-goal-h1{
                font-size: 36px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;
            }

            .about-goal-p{
                font-family: 'cschatthaiUI' ;

                font-size: 26px;
                line-height: 36x;
            }

            .about-goal-p-description {
                color: #191919;
                font-size: 20px;
                line-height: 26px;

                margin-top: -16px;
                padding-bottom: 15px;

            }

            .about-story-p-description {
                font-size: 20px;
                line-height: 25px;
                font-weight: 100;
            }

            .atmosphere{
                background-color: #27222A;
            }

            .about-atmosphere-h1 {
                font-size: 46px;
                font-family: 'cschatthaiUI' ;
                line-height: 63px;
                color: #fff;
            }

            .atmosphere-cover-img {
                width: 323px;
                height: 329px;
            }

            .about-bg-top{
                position: absolute;
                top: 0;
                left: 0;

                z-index: -1;

                width: 133px;
                height: 133px;
                background-repeat: no-repeat; /* Do not repeat the image */
                background-image: url({{ asset('img/home/slogan-bg-1.png') }});

            }

            .about-bg-bottom {
                position: absolute;
                right: 0px;
                bottom: 0px;
                z-index: -1;
                width: 195px;
                height: 209px;
                background-repeat: no-repeat;
                background-image: url({{ asset('img/home/slogan-bg-2.png') }});
            }

        }

        .atmosphere .owl-nav, .atmosphere .owl-dots {
            padding-top: 60px;
            text-align: center;
        }

        .atmosphere .owl-dot {
            display: inline-block;
            margin: 0 5px;
            width: 5px;
            height: 5px;
            border-radius: 50%;
            background-color: #ddd !important;
        }

        .atmosphere .owl-dot.active {
            background-color: #C4C4C4 !important;
        }

        @if( $title_agent == 'isDesktop')

            *, *::before, *::after {
                padding-top: 0;
            }

            .about-header{
                padding-top: 200px;
            }

            .about-img-bg{
                background-image: url({{ asset('img/about/cover.jpg') }});
                width: 100%;
                height: 635px;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            .about-h1{
                font-size: 64px;
                font-family: 'cschatthaiUI' ;
                line-height: 88px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
            }

            .about-p{
                font-size: 40px;
                line-height: 51.8px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                /* padding: 0 15px ; */
            }

            #main{
                position: relative;
                left: 0;
                top: -5px;
            }

            .about-target-title{
                font-family: 'cschatthaiUI' ;
                font-size: 46px;
                line-height: 63px;
            }

            .about-target-li {
                color: #444444;

                font-size: 26px;
                line-height: 30px;
            }

            .slogan-mission-img{

                box-shadow:0 10px 16px 0 rgba(0,0,0,0.25),0 6px 20px 0 rgba(0,0,0,0.19) !important;
                border-radius: 10px;
            }

            .goal-bg{
                background-color: #F5F5F5 ;
            }

            .about-goal-p{
                font-family: 'cschatthaiUI' ;
                font-size: 26px;
                line-height: 35.79px;
            }

            .about-goal-p-description {
                color: #191919;
                font-size: 26px;
                line-height: 28px;
                margin-top: -16px;
            }

            .about-before-p-description {
                font-family: 'cschatthai';
                font-size: 26px;
                line-height: 30px;
            }

            .img-container {
                width: 100%;
                height: 100%;
                cursor: pointer;
                overflow: hidden;
            }
            .img-container:hover .img-content-hover {
                display: block;
            }

            .img-gallery {
                width: 100%;
                height: 100%;
                object-fit: cover;
                transform: scale(1);
                transition: all 0.3s ease-in-out;
            }
            .img-gallery:hover {
                transform: scale(1.05);
            }

        @endif

    </style>

@endsection

@section('content')

    <div class="container-fluid px-0 set-cover">
        @if( $title_agent == 'isDesktop')
            <div class="about-img-bg">
                <div class="container">
                    <div class="about-header text-white">
                        <h3 class="about-h1" data-aos="fade-up" >วิสัยทัศน์</h3>
                        <p class="about-p" data-aos="fade-up" >เราจะขับเคลื่อนธุรกิจด้วยความเชี่ยวชาญ ดำเนินงานภายใต้ความปลอดภัยได้รับ
                            ความเชื่อถือมากที่สุดจากลูกค้าและเราจะ พัฒนาธุรกิจอย่างไม่หยุดยั้งด้วย
                            เทคโนโลยี เพื่อให้ลูกค้าทั่วประเทศไทย ได้รับบริการ อย่างเหนือความคาดหมาย</p>
                    </div>
                </div>
            </div>
        @endif
        @if( $title_agent == 'isMobile')
            <div class="about-header about-img-bg text-white">
                <h3 class="about-h1 ">วิสัยทัศน์</h3>
                <p class="about-p">เราจะขับเคลื่อนธุรกิจด้วยความเชี่ยวชาญ ดำเนินงานภายใต้ความปลอดภัยได้รับ ความเชื่อถือมากที่สุดจากลูกค้าและเราจะ พัฒนาธุรกิจอย่างไม่หยุดยั้งด้วยเทคโนโลยี เพื่อให้ลูกค้าทั่วประเทศไทย ได้รับบริการ อย่างเหนือความคาดหมาย</p>
            </div>
        @endif

    </div>

    <main id="main">
    @if( $title_agent == 'isDesktop')
        <div class="container">
            <div class="row">
                <div class="col-12 mt-4 py-2">
                    <h2 class="about-target-title">พันธกิจ</h2>
                </div>
                <div class="col-6">
                    <div class="slogan-mission-img">
                      <img src="{{ asset('img/about/big-img-1.jpg') }}" class="card-img-top">
                    </div>
                </div>
                <div class="col-6">
                    <p class="about-target-li my-4" data-aos="fade-up">พนักงานของเราต้องได้รับการฝึกอบรมตามหลักสูตรที่เป็นมาตราฐานและมีการทดสอบเพื่อให้มั่นใจว่าพนักงานพร้อมที่จะให้บริการ และปฏิบัติ งานได้อย่างเชี่ยวชาญ บริษัทของเรายังส่งเสริมให้ พนักงานรู้จักการพัฒนาตนเอง และเรียนรู้ให้เท่าทันเทคโนโลยีที่เปลี่ยนแปลงอยู่เสมอ</p>
                    <p class="about-target-li my-4" data-aos="fade-up">บริษัทของเรามุ่งมั่นในการสร้างความปลอดภัย ทั้งด้านการบริหารจัดการข้อมูลหรือเอกสารของลูกค้าภายใต้ พ.ร.บ.คุ้มครองข้อมูลส่วนบุคคล (PDPA)และด้านความปลอดภัยเกี่ยวการปฏิบัติ หน้าที่ของพนักงาน</p>
                    <p class="about-target-li my-4" data-aos="fade-up">บริษัทให้ความสำคัญกับนวัฒกรรมและเทคโนโลยีใหม่ๆ อยู่เสมอโดยสนับสนุนฝ่าย เทคโนโลยีสารสนเทศให้พัฒนาระบบขึ้นมา เพื่อเพิ่มประสิทธิภาพในการทำงานลดค่าใช้จ่ายและจำกัดความเสี่ยง ให้แก่บริษัทและลูกค้า</p>
                </div>
            </div>
        </div>

        <div class="container-fluid goal-bg my-5">
            <div class="row">
                <div class="col-12 mt-4 py-5">
                    <h2 class="about-target-title text-center py-3" data-aos="fade-up" >เป้าหมาย</h2>
                </div>
            </div>
            <div class="container">
                <div class="row text-center" data-aos="fade-up" >
                    <div class="col-4">
                        <img src="{{ asset('img/about/icon-1.svg') }}" class="img-fluid">
                        <p class="about-goal-p mt-2">ขับเคลื่อนองค์กร</p>
                        <p class="about-goal-p-description">ให้เติบโตอย่างไม่หยุดยั้ง <br/> โดยการสร้างคนไปพร้อมกับการสร้างแบรนด์</p>
                    </div>
                    <div class="col-4">
                        <img src="{{ asset('img/about/icon-2.svg') }}" class="img-fluid">
                        <p class="about-goal-p mt-2">ขยายกลุ่มลูกค้ามากขึ้น</p>
                        <p class="about-goal-p-description">นอกหนือจากลูกค้ากลุ่มสถานบันการเงินโดย <br/> จับกลุ่มตลาดลูกค้ารายเดือนมากขึ้น</p>
                    </div>
                    <div class="col-4">
                        <img src="{{ asset('img/about/icon-3.svg') }}" class="img-fluid">
                        <p class="about-goal-p mt-2">พัฒนาเทคโนโลยี</p>
                        <p class="about-goal-p-description">เพื่อรองรับความต้องการของลูกค้า</p>
                    </div>
                    <div class="col-12 my-4"></div>
                    <div class="col-6 pb-5">
                        <img src="{{ asset('img/about/icon-4.svg') }}" class="img-fluid">
                        <p class="about-goal-p mt-2">สร้างวัฒนธรรมองค์กร</p>
                        <p class="about-goal-p-description">ให้พนักงานทุกคนมีความสุขในการทำงานรู้จัก<br>คุณค่าของตนเองและพัฒนาศักยภาพของตนเองอยู่เสมอ</p>
                    </div>
                    <div class="col-6 pb-5">
                        <img src="{{ asset('img/about/icon-5.svg') }}" class="img-fluid">
                        <p class="about-goal-p mt-2">นำองค์กรสู่ตลาดหลักทรัพย์</p>
                        <p class="about-goal-p-description">แห่งประเทศไทยและให้พนักงานทุกคน<br/>มีส่วนร่วมเป็นผู้ถือหุ้นของบริษัท</p>
                    </div>
                </div>
            </div>
        </div>

        <div class="container my-5">
            <div class="row">

                <div class="col-12">
                    <h2 class="about-target-title" data-aos="fade-up" >จากวันนั้นถึงวันนี้</h2>
                </div>

                <div class="col-12 mb-5">
                    <p class="about-before-p-description" data-aos="fade-up" >
                        บริษัทของเราเริ่มต้นจากเป้าหมายแรกที่ต้องการให้
                        แมสเซ็นเจอร์มีรายได้มากกว่าที่คนอื่นเห็นและสร้าง
                        มาตราฐานความเป็นมืออาชีพที่เป็นมากกว่าพนักงาน
                        รับส่งเอกสารทั่วไป ไม่ใช่แค่การส่งเอกสารแต่เป็นการ
                        ส่งมอบบริการที่มีคุณภาพและสร้างความพึงพอใจ
                        โดยมุ่งเน้นให้บริการแก่กลุ่มสถาบันการเงินเป้าหมายนี้
                        ถือเป็นก้าวแรกของการก่อตั้งบริษัทขึ้นในปีพ.ศ.2549
                        ณ ปัจจุบันบริษัท ไพวอท จำกัด มีสำนักงานใหญ่
                        ตั้งอยู่ที่ศูนย์กลางย่านธุรกิจอย่างซอยสุขุมวิท 55
                        หรือซอยทองหล่อมีสาขา และพนักงานกว่า 800 คน
                        ครอบคลุมทั่วประเทศไทยนอกจากนี้บริษัทของเรา
                        ยังคงพัฒนาอย่างต่อเนื่องโดยขยายธุรกิจด้านบริการ
                        ต่างๆ รวมถึงการนำเอานวัฒกรรมใหม่ๆ เข้ามา
                        ประยุกต์ใช้งานสร้างระบบการจัดการเพิ่มประสิทธิภาพ
                        และตอบสนองความต้องการของลูกค้า
                    </p>
                </div>

            </div>

            <div class="row"data-aos="fade-up">
                <div class="col-12 col-md-4 col-xs-12">
                    <figure class="img-container">
                        <img src="{{ asset('img/about/today-1.jpg') }}" class="img-gallery">
                    </figure>
                </div>
                <div class="col-12 col-md-4 col-xs-12">
                    <figure class="img-container">
                        <img src="{{ asset('img/about/today-2.jpg') }}" class="img-gallery">
                    </figure>
                </div>
                <div class="col-12 col-md-4 col-xs-12">
                    <figure class="img-container">
                        <img src="{{ asset('img/about/today-3.jpg') }}" class="img-gallery">
                    </figure>
                </div>
            </div>

            <div class="row pt-4" data-aos="fade-up">
                <div class="col-12 col-md-6 col-xs-12">
                    <figure class="img-container">
                        <img src="{{ asset('img/about/today-4.jpg') }}" class="img-gallery">
                    </figure>
                </div>

                <div class="col-12 col-md-6 col-xs-12">
                    <figure class="img-container">
                        <img src="{{ asset('img/about/today-5.jpg') }}" class="img-gallery">
                    </figure>
                </div>
            </div>

            <div class="row pt-4" data-aos="fade-up">
                <div class="col-12">
                    <figure class="img-container">
                        <img src="{{ asset('img/about/today-6.png') }}" class="img-gallery">
                    </figure>
                </div>
            </div>
        </div>

    @endif

    @if( $title_agent == 'isMobile')

        <!-- ======= Fixed Background ======= -->

        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="about-bg-top"></div>
                </div>
            </div>
        </div>

        <!-- End Fixed Background -->

        <div id="portfolio" class="portfolio">
            <div class="container pt-5">
              <p class="about-target-title">พันธกิจ</p>
                <p class="about-target-li" data-aos="fade-up">พนักงานของเราต้องได้รับการฝึกอบรมตามหลักสูตรที่เป็นมาตราฐานและมีการทดสอบเพื่อให้มั่นใจว่าพนักงานพร้อมที่จะให้บริการ และปฏิบัติ งานได้อย่างเชี่ยวชาญ บริษัทของเรายังส่งเสริมให้ พนักงานรู้จักการพัฒนาตนเอง และเรียนรู้ให้เท่าทันเทคโนโลยีที่เปลี่ยนแปลงอยู่เสมอ</p>
                <p class="about-target-li" data-aos="fade-up">บริษัทของเรามุ่งมั่นในการสร้างความปลอดภัย ทั้งด้านการบริหารจัดการข้อมูลหรือเอกสารของลูกค้าภายใต้ พ.ร.บ.คุ้มครองข้อมูลส่วนบุคคล (PDPA)และด้านความปลอดภัยเกี่ยวการปฏิบัติ หน้าที่ของพนักงาน</p>
                <p class="about-target-li" data-aos="fade-up">บริษัทให้ความสำคัญกับนวัฒกรรมและเทคโนโลยีใหม่ๆ อยู่เสมอโดยสนับสนุนฝ่าย เทคโนโลยีสารสนเทศให้พัฒนาระบบขึ้นมา เพื่อเพิ่มประสิทธิภาพในการทำงานลดค่าใช้จ่ายและจำกัดความเสี่ยง ให้แก่บริษัทและลูกค้า</p>


              <div class="row portfolio-container">
                <div class="col-lg-4 col-md-6 portfolio-item filter-card pb-4" data-aos="fade-up">
                    <div class="portfolio-wrap slogan-mission-img">
                      <img src="{{ asset('img/about/mission-1.png') }}" class="card-img-top">
                    </div>
                </div>
              </div>

            </div>
        </div>

        <!-- ======= Fixed Background ======= -->

            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="about-bg-bottom"></div>
                    </div>
                </div>
            </div>

        <!-- End Fixed Background -->

        <div id="goal" class="goal">
            <div class="container">

                <div data-aos="fade-up">
                    <h3 class="about-goal-h1 pl-3">เป้าหมาย</h3>
                </div>

                <div class="row pl-3">

                    <div class="col-xl d-flex align-items-stretch pt-4 pt-xl-0" >
                      <div class="content d-flex flex-column justify-content-center">
                        <div class="row">

                          <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                              <img src="{{ asset('img/about/icon-1.svg') }}" class="img-fluid">
                              <p class="about-goal-p mt-2">ขับเคลื่อนองค์กร</p>
                              <p class="about-goal-p-description">ให้เติบโตอย่างไม่หยุดยั้ง <br/> โดยการสร้างคนไปพร้อมกับการสร้างแบรนด์</p>
                          </div>

                          <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                            <img src="{{ asset('img/about/icon-2.svg') }}" class="img-fluid">
                            <p class="about-goal-p mt-2">ขยายกลุ่มลูกค้ามากขึ้น</p>
                            <p class="about-goal-p-description">นอกหนือจากลูกค้ากลุ่มสถานบันการเงินโดย <br/> จับกลุ่มตลาดลูกค้ารายเดือนมากขึ้น</p>
                          </div>

                          <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                              <img src="{{ asset('img/about/icon-3.svg') }}" class="img-fluid">
                              <p class="about-goal-p mt-2">พัฒนาเทคโนโลยี</p>
                              <p class="about-goal-p-description">เพื่อรองรับความต้องการของลูกค้า</p>
                          </div>

                          <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                              <img src="{{ asset('img/about/icon-4.svg') }}" class="img-fluid">
                              <p class="about-goal-p mt-2">สร้างวัฒนธรรมองค์กร</p>
                              <p class="about-goal-p-description">ให้พนักงานทุกคนมีความสุขในการทำงานรู้จักคุณค่าของตนเองและพัฒนาศักยภาพของตนเองอยู่เสมอ</p>
                          </div>

                          <div class="col-md-6 d-md-flex align-items-md-stretch" data-aos="fade-up">
                              <img src="{{ asset('img/about/icon-5.svg') }}" class="img-fluid">
                              <p class="about-goal-p mt-2">นำองค์กรสู่ตลาดหลักทรัพย์</p>
                              <p class="about-goal-p-description">แห่งประเทศไทยและให้พนักงานทุกคน<br/>มีส่วนร่วมเป็นผู้ถือหุ้นของบริษัท</p>
                          </div>
                        </div>
                      </div><!-- End .content-->
                    </div>
                  </div>
            </div>
        </div>

        <div id="story" class="story">
            <!-- ======= Fixed Background ======= -->

            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="about-bg-top"></div>
                    </div>
                </div>
            </div>

            <!-- End Fixed Background -->

            <div class="container pt-5 pb-2">

                <div data-aos="fade-up">
                    <h3 class="about-goal-h1 pl-3">จากวันนั้นถึงวันนี้</h3>
                </div>

                <div class="row pl-3">
                    <div class="col-xl d-flex align-items-stretch pt-4 pt-xl-0" data-aos="fade-up">
                      <div class="content d-flex flex-column justify-content-center">
                        <div class="row">

                          <div class="col-md-6 d-md-flex align-items-md-stretch">
                              <p class="about-goal-p-description pb-5">บริษัทของเราเริ่มต้นจากเป้าหมายแรกที่ต้องการให้
                                แมสเซ็นเจอร์มีรายได้มากกว่าที่คนอื่นเห็นและสร้าง
                                มาตราฐานความเป็นมืออาชีพที่เป็นมากกว่าพนักงาน
                                รับส่งเอกสารทั่วไป ไม่ใช่แค่การส่งเอกสารแต่เป็นการ
                                ส่งมอบบริการที่มีคุณภาพและสร้างความพึงพอใจ
                                โดยมุ่งเน้นให้บริการแก่กลุ่มสถาบันการเงินเป้าหมายนี้
                                ถือเป็นก้าวแรกของการก่อตั้งบริษัทขึ้นในปีพ.ศ.2549
                                ณ ปัจจุบันบริษัท ไพวอท จำกัด มีสำนักงานใหญ่
                                ตั้งอยู่ที่ศูนย์กลางย่านธุรกิจอย่างซอยสุขุมวิท 55
                                หรือซอยทองหล่อมีสาขา และพนักงานกว่า 800 คน
                                ครอบคลุมทั่วประเทศไทยนอกจากนี้บริษัทของเรา
                                ยังคงพัฒนาอย่างต่อเนื่องโดยขยายธุรกิจด้านบริการ
                                ต่างๆ รวมถึงการนำเอานวัฒกรรมใหม่ๆ เข้ามา
                                ประยุกต์ใช้งานสร้างระบบการจัดการเพิ่มประสิทธิภาพ
                                และตอบสนองความต้องการของลูกค้า</p>
                          </div>

                      </div><!-- End .content-->
                    </div>
                  </div>
                </div>
        </div>

        <!-- ======= Fixed Background ======= -->

        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="about-bg-bottom"></div>
                </div>
            </div>
        </div>

        <!-- End Fixed Background -->

        <div id="atmosphere" class="atmosphere">
            <div class="container px-4">
                <div data-aos="fade-up" >
                    <h3 class="about-atmosphere-h1 pl-3">บรรยากาศ</h3>
                </div>

                <div class="owl-carousel testimonials-carousel">

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                            <img src="{{ asset('img/about/about-img-1.jpg') }}" class="w-100" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/about/about-img-2.jpg') }}" class="w-100" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/about/about-img-3.jpg') }}" class="w-100" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/about/about-img-4.jpg') }}" class="w-100" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/about/about-img-5.jpg') }}" class="w-100" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/about/about-img-6.jpg') }}" class="w-100" alt="">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif

@endsection
