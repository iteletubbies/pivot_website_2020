@extends("$frontend")

@section('title-agent')
    {{-- {{ $title_agent }} --}}
    {{ config('app.name')}}
@endsection

@section('style-header')
<style>
.card-img-top{
     border:0px;
     border-radius: 8px !important;
 }
 .card {
 border-radius: 8px;
 }


    @media (max-width: 768px) {
        .slogan-bg-top {
            position: absolute;
            top: 550px;
            left: 0;
            z-index: 1 !important;
        }
        .more-services .card {
            border: 0;
            padding: 20px 20px 20px 20px !important;
            border-radius: 10px;
            position: relative;
            border-radius: 10px;
            width: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
        }

       .card-body {
            z-index: 10;
            /* background: rgba(255, 255, 255, 0.9); */
            padding: 15px 30px;
            /* box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.1); */
            transition: 0.3s;
            transition: ease-in-out 0.4s;
            border-radius: 5px;
        }

        .card-img-top{
            border-radius: 10px;
        }

        .service-h2-1{
            font-size: 26px;
            font-family: 'cschatthai';
            font-weight: 600;
            /* padding: 0 15px; */
            color: #191919;
        }
        .service-h2-1-title{
            padding-top: 20px;
        }
        .service-p-1 {
            font-size: 20px;
            font-family: 'cschatthai' ;
            /* padding: 0 15px ; */
            color:#191919;
        }
        .service-p-2 {
            font-size: 20px;
            font-family: 'cschatthai' ;
            /* padding: 0 15px ; */
            color:red;
            margin-top: -15px;
        }



   }

*, *::before, *::after {
    padding-top: 0px;
}


.carousel-inner .carousel-item.active,
    .carousel-inner .carousel-item-next,
    .carousel-inner .carousel-item-prev {
        display: flex;
    }

    /* display 3 */
    @media (min-width: 768px) {

        .carousel-inner .carousel-item-right.active,
        .carousel-inner .carousel-item-next {
          transform: translateX(33.333%);
        }

        .carousel-inner .carousel-item-left.active,
        .carousel-inner .carousel-item-prev {
          transform: translateX(-33.333%);
        }
    }

        .carousel-inner .carousel-item-right,
        .carousel-inner .carousel-item-left {
              transform: translateX(0);
        }


        .slogan {
            position: relative;
            width: 100%;
            min-width: 100%;
            height: auto;
            margin-bottom:30px;
        }
     @media (max-width: 768px) {

         .slogan {
            position: relative;
            width: 100%;
            min-width: 100%;
            height: auto;
            min-height: 100%;
            margin-bottom:30px;
        }

        .slogan-bg-top{
            position: absolute;
            top: px;
            left: 0px;
            z-index: -1;
            width:30%;
            height30%;
        }

        .slogan-title{
            font-family: cschatthai !important;
            font-size:32px;
            font-weight:bold;
            text-align:left;
            margin-top: -28px;
            margin-left: 15px;
            margin-bottom: 0px;
        }

        .card-img-top{
            border:0px;
            border-radius: 10px !important;
        }
        .slogan-img{
            box-shadow: 0 10px 16px 0 rgba(0,0,0,0.2),0 4px 20px 0 rgba(0,0,0,0.19) !important;
            border-radius: 15px;
        }
        .slogan-bg-bottom{
            position: absolute;
            right: 0px;
            bottom: -30px;
            {# z-index: -1; #}
            width:40%;
            height40%;
        }

     }

     .cover_home{

         background-size: cover;
        background-position: center;
        object-fit: cover;
     }

</style>

@endsection

@section('content')
{{-- isDesktop --}}
{{-- isDesktop --}}
{{-- isDesktop --}}
{{-- isDesktop --}}
@if ($title_agent === 'isDesktop')
<section id="testimonials" class="">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            </ol>
            <div class="carousel-inner">

                <div class="carousel-item active cover_home" style="background-image: url({{ asset('img_desktop/home/cover-2.jpg') }});">
                   <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">

                   </a>
                </div>
                <div class="carousel-item cover_home" style="background-image: url({{ asset('img_desktop/home/cover-1.jpg') }});">
                </div>
             </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
</section>
@else
{{-- isMobile --}}
{{-- isMobile --}}
{{-- isMobile --}}
{{-- isMobile --}}
<section id="testimonials" class="testimonials section-bg">
    <div class="container-fluid">

        <div class="owl-carousel testimonials-carousel">

            <div class="testimonial-wrap">
              <div class="testimonial-item">
              <img src="{{ asset('img/home/cover-1.jpg') }}" class="home-cover-img" alt="">
              </div>
            </div>

            <div class="testimonial-wrap">
              <div class="testimonial-item">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                    <img src="{{ asset('img/home/cover-2.jpg') }}" class="home-cover-img" alt="">
                </a>
              </div>
            </div>

            {{-- <div class="testimonial-wrap">
              <div class="testimonial-item">
                <img src="{{ asset('img/home/cover-1.png') }}" class="home-cover-img" alt="">
              </div>
            </div>

            <div class="testimonial-wrap">
              <div class="testimonial-item">
                <img src="{{ asset('img/home/cover-1.png') }}" class="home-cover-img" alt="">
              </div>
            </div> --}}

          </div>
        </div>

  </section>
  @endif

  <main id="main">

<section id="portfolio" class="portfolio">
    <div class="container">

      @if ($title_agent == 'isDesktop')
        <img src="{{ asset('img/home/slogan-bg-1.svg') }}" class="slogan-bg-top">
        <img src="{{ asset('img/home/slogan-bg-2.svg') }}" class="slogan-bg-bottom">
      @else
        <img src="{{ asset('img/home/slogan-bg-1.png') }}" class="slogan-bg-top">
      @endif
      <p class="slogan-title">SLOGAN</p>
      <div class="row portfolio-container">

          <div class="col-lg-4 col-md-4 portfolio-item filter-card" data-aos="fade-up" >
              <div class="portfolio-wrap slogan-img">
                <img src="{{ asset('img/home/slogan-1.jpg') }}" class="card-img-top" alt="professional">
              </div>
          </div>

          <div class="col-lg-4 col-md-4 portfolio-item filter-card" data-aos="fade-up" >
              <div class="portfolio-wrap slogan-img">
                <img src="{{ asset('img/home/slogan-2.jpg') }}" class="card-img-top" alt="Safety Sense">
              </div>
          </div>

          <div class="col-lg-4 col-md-4 portfolio-item filter-card" data-aos="fade-up" >
              <div class="portfolio-wrap slogan-img">
                <img src="{{ asset('img/home/slogan-3.jpg') }}" class="card-img-top" alt="High Technology">
              </div>
          </div>

      </div>

    </div>
  </section><!-- End More Services Section -->

 <!-- ======= Fixed Background ======= -->
 @if ($title_agent != 'isDesktop')
 <div class="container">
    <div class="row">
        <div class="col">
            <div class="slogan-bg-bottom"></div>
        </div>
    </div>
  </div>
  @endif

  <!-- End Fixed Background -->

  <!-- ======= More Services Section ======= -->
  <section id="more-services" class="more-services">

    <div class="container">

         <h2 class="service-title" data-aos="fade-up">บริการ</h2>

      <div class="row content text-white">

          <div class="col-lg-12" data-aos="fade-up">

            <p class="service-h2">ธุรกิจของไพวอทฯ นอกจากจะให้บริการพนักงานรับส่งเอกสารแล้ว เรายังมีบริการ อื่นๆและระบบการจัดการที่ครอบคลุมสู่การเป็นผู้ให้บริการด้านรับส่งเอกสารแบบครบวงจร โดยเน้นเรื่องความเชี่ยวชาญ ความปลอดภัยและเทคโนโลยี เพื่อส่งมอบ บริการที่มีคุณภาพและสร้างความพึงพอใจอย่างเหนือความคาดหมายให้แก่ลูกค้าของเรา</p>
          </div>
          @if ($title_agent != 'isDesktop')
        <div class="col-lg-4 col-md-4 mt-4" data-aos="fade-up">
            <div class="portfolio-wrap">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                    <div class="card" style="">
                        <img src="{{ asset('img/home/service-01.jpg') }}" class="card-img-top" alt="">
                        <h3 class="service-h2-1 service-h2-1-title">แมสเซ็นเจอร์แบบรายเดือน</h3>
                        <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร รับเช็ค วางบิล
                            เอกสารสำคัญประจำสำนักงานของท่าน<br>
                        อัตราเริ่มต้นที่ 16,500 บาท/เดือน</p>

                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 mt-4" data-aos="fade-up">
            <div class="portfolio-wrap">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                    <div class="card" style="">
                        <img src="{{ asset('img/home/service-02.jpg') }}" class="card-img-top" alt="">
                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายวัน</h3>
                        <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร
                            รับเช็ค วางบิลเอกสารสำคัญอัตราเริ่มต้นที่ 800 บาท/วัน</p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 mt-4" data-aos="fade-up">
            <div class="portfolio-wrap">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                    <div class="card" style="">
                        <img src="{{ asset('img/home/service-03.jpg') }}" class="card-img-top" alt="">
                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบเร่งด่วน</h3>
                        <p class="service-p-1">บริการรับส่งเอกสารแบบงานด่วน พัสดุที่ต้องการส่งแบบทันที
                            อัตราเริ่มต้นที่ 250 บาท<br></p>
                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 mt-4" data-aos="fade-up">
            <div class="portfolio-wrap">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                    <div class="card" style="">
                        <img src="{{ asset('img/home/service-04.jpg') }}" class="card-img-top" alt="">
                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายชิ้น</h3>
                        <p class="service-p-1">บริการรับส่งเอกสาร (สินค้า)
                            แบบรายชิ้น<br>อัตราเริ่มต้นที่ 95 บาท <br></p>
                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 mt-4" data-aos="fade-up">
            <div class="portfolio-wrap">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                    <div class="card" style="">
                        <img src="{{ asset('img/home/service-05.jpg') }}" class="card-img-top" alt="">
                        <h3 class="service-h2-1 service-h2-1-title ">จัดส่งสินค้าโดยรถยนต์</h3>
                        <p class="service-p-1">บริการจัดส่งสินค้าโดยรถยนต์ในกรุงเทพและต่างจังหวัดราคาเริ่มต้นที่ 1,000 บาท<br></p>
                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 mt-4 mb-5" data-aos="fade-up">
            <div class="portfolio-wrap">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                    <div class="card" style="">
                        <img src="{{ asset('img/home/service-06.jpg') }}" class="card-img-top" alt="">
                        <h3 class="service-h2-1 service-h2-1-title ">บริการจัดการคลังสินค้า</h3>
                        <p class="service-p-1">งานบริการเช่าคลังสินค้า และ บริการจัดการคลังสินค้า เช่น จัดเก็บสินค้าพรีเมี่ยม โดยมีระบบ
                            รองรับดูแลตลอดเวลา 24 ชม. อัตราเริ่มต้นที่
                            200 / ตรม</p>
                    </div>
                </a>
            </div>
        </div>
        @else
        </div>
    </div>
    <div class="container" data-aos="fade-up" data-aos-duration="1000">
        <div class="row">
            <div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel">
                <div class="carousel-inner w-100" role="listbox">
                    <div class="carousel-item active">
                            <div class="col col-12 col-md-4 col-sm-12 my-3">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                                <div class="card" style="height: 511px;">
                                    <img src="{{ asset('img/home/service-01.jpg') }}" class="card-img-top" alt="" >
                                    <h3 class="service-h2-1 pt-3">แมสเซ็นเจอร์แบบรายเดือน</h3>
                                    <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร รับเช็ค วางบิล
                                        เอกสารสำคัญประจำสำนักงานของท่านอัตราเริ่มต้นที่ 16,500 บาท/เดือน</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="col col-12 col-md-4 col-sm-12  my-3">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                            <div class="card" style="height: 511px;">
                                <img src="{{ asset('img/home/service-02.jpg') }}" class="card-img-top" alt="">
                                <h3 class="service-h2-1 pt-3">แมสเซ็นเจอร์แบบรายวัน</h3>
                                <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร
                                    รับเช็ค วางบิลเอกสารสำคัญ อัตราเริ่มต้นที่ 800 บาท/วัน</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="col col-12 col-md-4 col-sm-12 my-3">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                            <div class="card" style="height: 511px;">
                                <img src="{{ asset('img/home/service-03.jpg') }}" class="card-img-top" alt="">
                                <h3 class="service-h2-1 pt-3">แมสเซ็นเจอร์แบบเร่งด่วน</h3>
                                <p class="service-p-1">บริการรับส่งเอกสารแบบงานด่วน พัสดุที่ต้องการส่งแบบทันที
                                อัตราเริ่มต้นที่ 250 บาท<p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="col col-12 col-md-4 col-sm-12 my-3" >
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                            <div class="card" style="height: 511px;">
                                <img src="{{ asset('img/home/service-04.jpg') }}" class="card-img-top" alt="">
                                <h3 class="service-h2-1 pt-3">แมสเซ็นเจอร์แบบรายชิ้น</h3>
                                <p class="service-p-1">บริการรับส่งเอกสาร (สินค้า) แบบรายชิ้น อัตราเริ่มต้นที่ 95 บาท</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="col col-12 col-md-4 col-sm-12 my-3">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                            <div class="card" style="height: 511px;">
                                <img src="{{ asset('img/home/service-05.jpg') }}" class="card-img-top" alt="">
                                <h3 class="service-h2-1 pt-3">จัดส่งสินค้าโดยรถยนต์</h3>
                                <p class="service-p-1">บริการจัดส่งสินค้าโดยรถยนต์ในกรุงเทพและต่างจังหวัดราคาเริ่มต้นที่ 1,000 บาท</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="col col-12 col-md-4 col-sm-12 my-3">
                            <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank">
                            <div class="card" style="height: 511px;">
                                <img src="{{ asset('img/home/service-06.jpg') }}" class="card-img-top" alt="">
                                <h3 class="service-h2-1 pt-3">บริการจัดการคลังสินค้า</h3>
                                <p class="service-p-1">งานบริการเช่าคลังสินค้า และ บริการจัดการคลังสินค้า เช่น จัดเก็บสินค้าพรีเมี่ยมโดยมีระบบรองรับดูแลตลอดเวลา 24 ชม.<br>อัตราเริ่มต้นที่ 200 / ตรม</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    </div>
                    <a class="carousel-control-prev w-auto" href="#recipeCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next w-auto" href="#recipeCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
        @endif

  </section><!-- End Contact Section -->


@endsection

@if ($title_agent === 'isDesktop')
@section('jquery')
$('#recipeCarousel').carousel({
    interval: 5000
  })

  $('#recipeCarousel .carousel-item').each(function(){
      var minPerSlide = 3;
      var next = $(this).next();
      if (!next.length) {
      next = $(this).siblings(':first');
      }
      next.children(':first-child').clone().appendTo($(this));

      for (var i=0;i<minPerSlide;i++) {
          next=next.next();
          if (!next.length) {
              next = $(this).siblings(':first');
            }

          next.children(':first-child').clone().appendTo($(this));
        }
  });
@endsection
@endif
