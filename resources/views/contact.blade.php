@extends("$frontend")

@section('title-agent')
    {{-- {{ $title_agent }} --}}
    {{ config('app.name')}}
@endsection

@section('style-header')

    <style>
        .service-title{
            color: #000 !important;
        }
        @media (max-width: 768px) {

            .header-map {
                height: 300px;
                width: 100%;
                position: relative;
                margin:30px 30px 30px 30px;

            }

            .thailand-map-img{
                width:100%;
                height:100%;
                /* margin: 10px; */
            }

            .thailand-map-h1{
                font-size: 26px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;
            }

            .map-h3{
                font-size: 26px;
                font-family: 'cschatthaiUI' ;
                line-height: 36px;
            }

            .btn-primary{
                background: #FB872D;
                border-radius: 8px;
                font-size: 20px;
                color: #FFF;
                font-family:'cschatthaiUI';
                width: 100%;

                border: 0px;
            }

            .card {
                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
            }

            .about-bg-bottom{
                position: absolute;
                /* right: -48%; */
                right: 0px;
                top: 2600px;
                z-index: -1;
            }

            #thonglor-map, #muangthong-map, #bangna-map, #samutsakhon-map{
                height: 300px;
                width: 100%;
            }

        }

        @if ($title_agent === 'isDesktop')
        section {
            padding: 90px 0 0 0  !important;
            overflow: hidden;
        }
        @endif
    </style>

@endsection

@section('content')


    <section id="thailand-map" class="thailand-map">
        @if ($title_agent != 'isDesktop')
            <div class="container-fluid">
        @else
            <div class="container text-center">
                <br>
                <br>
                <br>
                <br>
        @endif
        <h2 class="service-title text-center">ไพวอทในประเทศไทย</h2>

            <div class="row">
                <div class="col">
                    <div class="thailand-map-img" id="thailand-map-img"></div>
                </div>
            </div>
        </div>
    </section>
    @if ($title_agent != 'isDesktop')
    <main id="main">

        <section id="map" class="map">
            <div class="container pb-5">

                <h3 class="map-h3">บริษัท ไพวอท จำกัด <br />(สำนักงานใหญ่)</h3>

                <div class="row">

                    <div class="col-xl d-flex align-items-stretch pt-4 pt-xl-0">
                        <div class="d-flex flex-column justify-content-center">
                          <div class="row">

                            <div class="col-md-6 d-md-flex align-items-md-stretch">

                                <div class="card mb-3">
                                    {{-- <div class="col">
                                        <div id="thonglor-map"></div>
                                    </div> --}}
                                    {{-- <img class="card-img-top" src="{{ asset('img/contact/mapthonglor.png') }}" class="img-fluid"> --}}
                                    <div class="card-img-top" id="thonglor-map">
                                        <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=13.742572%2C100.585757&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">บริษัท ไพวอท จำกัด (สำนักงานใหญ่)</h5>
                                        <p class="card-text">1000/67-74 อาคารลิเบอร์ตี้ พลาซ่าชั้น1-3
                                            ซอย สุขุมวิท 55(ซอยทองหล่อ)
                                            แขวงคลองตันเหนือเขต วัฒนา กทม. 10110</p>
                                            <a href="https://www.google.co.th/maps/place/13%C2%B044'33.3%22N+100%C2%B035'08.7%22E/@13.742572,100.583563,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d13.742572!4d100.585757" class="btn btn-primary">Google map</a></a>
                                    </div>
                                </div>

                            </div>

                          </div>
                        </div>
                    </div>
                </div>

                <div data-aos="fade-up">
                    <h3 class="map-h3">สาขากรุงเทพฯและปริมลฑล</h3>
                </div>

                <div class="row">

                    <div class="col-xl d-flex align-items-stretch pt-4 pt-xl-0" data-aos="fade-up">
                        <div class="d-flex flex-column justify-content-center">
                          <div class="row">

                            <div class="col-md-6 d-md-flex align-items-md-stretch">

                                <div class="card mb-3">
                                    <div class="card-img-top" id="muangthong-map">
                                        <iframe width="100%" height="321" id="gmap_canvas" src="https://maps.google.com/maps?q=13.9236001%2C100.5328688&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">บริษัท ไพวอท เมืองทอง</h5>
                                        <p class="card-text">นิศานาถ แก้วกลาง (หัวหน้าสาขา)
                                            53/188 หมู่บ้านลูกกอล์ฟพรีเมี่ยม
                                            หมู่ที่ 9 ซอย1/1 ต.บางพูด อ.ปากเกร็ด
                                            จ.นนทบุรี 11120
                                            เวลาทำการ 08.00 น. - 17.00 น.</p>
                                            <a href="https://www.google.co.th/maps/place/Pivot+%E0%B8%AA%E0%B8%B2%E0%B8%82%E0%B8%B2+%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%87%E0%B8%97%E0%B8%AD%E0%B8%87/@13.9236001,100.5306748,17z/data=!3m1!4b1!4m5!3m4!1s0x30e28386d3c26831:0x9b98f777213903ae!8m2!3d13.9236001!4d100.5328688" class="btn btn-primary">Google map</a>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 d-md-flex align-items-md-stretch">

                                <div class="card mb-3">
                                    <div class="card-img-top" id="bangna-map">
                                        <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=13.6153188%2C100.7535385&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">บริษัท ไพวอท บางนา (คลังสินค้า)</h5>
                                        <p class="card-text">เมธี ทิพย์เลิศ (ผู้จัดการคลังสินค้า)
                                            คุณอาทร ทองนิ่ม (หัวหน้าสาขา)
                                            โกดังพัฒนากิ่งแก้ว โครงการ4
                                            ห้องเลขที่ 19/5 ม.7 ถ.บางนา-ตราด กม.17.5
                                            ต.บางโฉลง อ.บางพลี จ.สมุทรปราการ 10540
                                            เวลาทำการ 08.00 น. - 17.00 น.</p>
                                            <a href="https://www.google.co.th/maps/place/Pivot/@13.6153432,100.7534995,21z/data=!4m13!1m7!3m6!1s0x0:0x0!2zMTPCsDM2JzU1LjMiTiAxMDDCsDQ1JzEyLjciRQ!3b1!8m2!3d13.615353!4d100.753536!3m4!1s0x311d5c5f79e2fbc9:0x6fa20dd96d802be!8m2!3d13.6153188!4d100.7535385" class="btn btn-primary">Google map</a>
                                    </div>
                                </div>

                            </div>

                            <div class="col-md-6 d-md-flex align-items-md-stretch">

                                <div class="card mb-3">
                                    <div class="card-img-top" id="samutsakhon-map">
                                        <iframe width="100%" height="100%" id="gmap_canvas" src="https://maps.google.com/maps?q=13.702495%2C100.33152&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">บริษัท ไพวอท สาขาสมุทรสาคร</h5>
                                        <p class="card-text">วิโรจน์ ออเขาย้อย (หัวหน้าสาขา)
                                            5/638 ต.อ้อมน้อย อ.กระทุ่มแบน จ.สมุทรสาคร 74130
                                            เวลาทำการ 08.00 น. - 17.00 น.</p>
                                            <a href="https://www.google.com/maps/place/13%C2%B042'09.0%22N+100%C2%B019'53.5%22E/@13.7024076,100.3316452,20z/data=!4m5!3m4!1s0x0:0x0!8m2!3d13.702495!4d100.33152" class="btn btn-primary">Google map</a>
                                    </div>
                                </div>

                            </div>

                          </div>
                        </div>
                    </div>
                </div>

                <img src="{{ asset('img/home/slogan-bg-2.png') }}" class="about-bg-bottom">
            </div>
        </section>
    </main>
    @else
    <main id="main">
        <section id="map_desktop" class="map">
            <div class="container">
                <!-- บริษัท ไพวอท จำกัด (สำนักงานใหญ่)  -->
                <!-- บริษัท ไพวอท จำกัด (สำนักงานใหญ่)  -->
                <!-- บริษัท ไพวอท จำกัด (สำนักงานใหญ่)  -->

                <div class="row">
                    <div class="col-md-12" data-aos="fade-up" >
                        <div data-aos="fade-up">
                            <h3 class="map-h3">บริษัท ไพวอท จำกัด (สำนักงานใหญ่)</h3>
                        </div>
                        <div class="map_card_desktop card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-6 col-sm-12">
                                    <iframe width="100%" height="321" id="gmap_canvas" src="https://maps.google.com/maps?q=13.742572%2C100.585757&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="card-body">
                                        <h5 class="card-title">บริษัท ไพวอท จำกัด (สำนักงานใหญ่)</h5>
                                        <p class="card-text">1000/67-74 อาคารลิเบอร์ตี้ พลาซ่าชั้น1-3
                                            ซอย สุขุมวิท 55(ซอยทองหล่อ)
                                            แขวงคลองตันเหนือเขต วัฒนา กทม. 10110</p>
                                            <a href="https://www.google.co.th/maps/place/13%C2%B044'33.3%22N+100%C2%B035'08.7%22E/@13.742572,100.583563,17z/data=!3m1!4b1!4m5!3m4!1s0x0:0x0!8m2!3d13.742572!4d100.585757" class="btn btn-primary">Google map</a></a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <!-- สาขากรุงเทพฯและปริมลฑล -->
                <!-- สาขากรุงเทพฯและปริมลฑล -->
                <!-- สาขากรุงเทพฯและปริมลฑล -->
                <!-- สาขากรุงเทพฯและปริมลฑล -->


                <div class="row">
                    <div class="col-md-12" data-aos="fade-up" >
                        <div data-aos="fade-up">
                            <br>
                            <br>
                            <br>
                            <h3 class="map-h3">สาขากรุงเทพฯและปริมลฑล</h3>
                        </div>
                        <div class="map_card_desktop card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-6 col-sm-12">
                                <iframe width="100%" height="321" id="gmap_canvas" src="https://maps.google.com/maps?q=13.9236001%2C100.5328688&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>

                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="card-body">
                                    <h5 class="card-title">บริษัท ไพวอท เมืองทอง</h5>
                                    <p class="card-text">นิศานาถ แก้วกลาง (หัวหน้าสาขา)
                                        53/188 หมู่บ้านลูกกอล์ฟพรีเมี่ยม
                                        หมู่ที่ 9 ซอย1/1 ต.บางพูด อ.ปากเกร็ด
                                        จ.นนทบุรี 11120
                                        เวลาทำการ 08.00 น. - 17.00 น.</p>
                                        <a href="https://www.google.co.th/maps/place/Pivot+%E0%B8%AA%E0%B8%B2%E0%B8%82%E0%B8%B2+%E0%B9%80%E0%B8%A1%E0%B8%B7%E0%B8%AD%E0%B8%87%E0%B8%97%E0%B8%AD%E0%B8%87/@13.9236001,100.5306748,17z/data=!3m1!4b1!4m5!3m4!1s0x30e28386d3c26831:0x9b98f777213903ae!8m2!3d13.9236001!4d100.5328688" class="btn btn-primary">Google map</a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12" data-aos="fade-up" >
                        <div class="map_card_desktop card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-6 col-sm-12">
                                <iframe width="100%" height="321" id="gmap_canvas" src="https://maps.google.com/maps?q=13.6153188%2C100.7535385&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="card-body">
                                    <h5 class="card-title">บริษัท ไพวอท บางนา (คลังสินค้า)</h5>
                                    <p class="card-text">เมธี ทิพย์เลิศ (ผู้จัดการคลังสินค้า)
                                        คุณอาทร ทองนิ่ม (หัวหน้าสาขา)
                                        โกดังพัฒนากิ่งแก้ว โครงการ4
                                        ห้องเลขที่ 19/5 ม.7 ถ.บางนา-ตราด กม.17.5
                                        ต.บางโฉลง อ.บางพลี จ.สมุทรปราการ 10540
                                        เวลาทำการ 08.00 น. - 17.00 น.</p>
                                        <a href="https://www.google.co.th/maps/place/Pivot/@13.6153432,100.7534995,21z/data=!4m13!1m7!3m6!1s0x0:0x0!2zMTPCsDM2JzU1LjMiTiAxMDDCsDQ1JzEyLjciRQ!3b1!8m2!3d13.615353!4d100.753536!3m4!1s0x311d5c5f79e2fbc9:0x6fa20dd96d802be!8m2!3d13.6153188!4d100.7535385" class="btn btn-primary">Google map</a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12" data-aos="fade-up" >
                        <div class="map_card_desktop card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-6 col-sm-12">
                                <iframe width="100%" height="321" id="gmap_canvas" src="https://maps.google.com/maps?q=13.702495%2C100.33152&t=&z=17&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="card-body">
                                    <h5 class="card-title">บริษัท ไพวอท สาขาสมุทรสาคร</h5>
                                    <p class="card-text">วิโรจน์ ออเขาย้อย (หัวหน้าสาขา)
                                        5/638 ต.อ้อมน้อย อ.กระทุ่มแบน จ.สมุทรสาคร 74130
                                        เวลาทำการ 08.00 น. - 17.00 น.</p>
                                        <a href="https://www.google.com/maps/place/13%C2%B042'09.0%22N+100%C2%B019'53.5%22E/@13.7024076,100.3316452,20z/data=!4m5!3m4!1s0x0:0x0!8m2!3d13.702495!4d100.33152" class="btn btn-primary">Google map</a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>


    </main>
    @endif
    </div>
@endsection




@section('header-script')

@endsection

@section('footer-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.5.7/lottie.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function(){
        var animMap = lottie.loadAnimation({
            container: document.getElementById('thailand-map-img'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: '{{ asset('img/contact/map/Map-1.json') }}', // the animation data
            rendererSettings: {
                preserveAspectRatio: 'xMinYMin slice', // Supports the same options as the svg element's preserveAspectRatio property
                progressiveLoad: true, // Boolean, only svg renderer, loads dom elements when needed. Might speed up initialization for large number of elements.
            }
        });
    });



</script>

@endsection
