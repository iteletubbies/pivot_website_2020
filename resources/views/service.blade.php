@extends("$frontend")

@section('title-agent')
    {{-- {{ $title_agent }} --}}
    {{ config('app.name')}}
@endsection

@section('style-header')
<style>

    @media (max-width: 768px) {

        .service-header{
            padding: 90px 30px 0 30px ;
        }

        .service-img-bg{

            background-image: url('{{ asset('img/service/small-cover.jpg') }}');
            width: 100%;
            height: 500px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .service-h1{
            font-size: 36px;
            font-family: 'cschatthaiUI' ;
            line-height: 50px;

            text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
        }

        .service-p{
            font-size: 26px;
            line-height: 34px;

            text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            /* padding: 0 15px ; */
        }


        #more-services{
            background-color: #27222A;
            padding: 0px 15px;
        }

        .more-services-1{
            /* margin-top: 25px; */
            background-color: #27222A;
            padding: 0px 15px;
        }

        .promotion-title{
            font-size: 36px;
            line-height: 50px;
            font-family: 'cschatthaiUI' ;
        }

        .slogan-bg-top-services {
            position: absolute;
            top: 2932px;
            left: 0;
        }

        .slogan-bg-bottom-services{
            position: absolute;
            right: 0px;
            top: 3413px;
            z-index: -1;
        }

        .card-shadow{
            border: 1px solid #C4C4C4;
            box-sizing: border-box;
            box-shadow: 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19) !important;
            border-radius: 10px;
        }

        .card-title{
            font-family:'cschatthaiUI';
            font-size: 20px;
            line-height: 23px;
            font-weight: bold;
            text-align: center;
            padding-top: 10px;
            color:#191919;
        }

        .card-p{
            font-size: 16px;
            line-height: 25px;
        }

        .btn-service{
            background: #FB872D;
            border-radius: 8px;
            font-size: 20px;
            color: #FFF;
            font-family:'cschatthaiUI';
            width: 100%;
        }

        .carousel-indicators > li {
            border-radius: 50%;
            width: 5px;
            height: 5px;
            background-color: #404040;

            position: relative;
            top: 60px;
            left: 0px;

            /* margin-top: 260px; */
        }

        .service-bg-top {
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: -10;
            width: 195px;
            height: 209px;
            background-repeat: no-repeat;
            background-image: url({{ asset('img/home/slogan-bg-1.png') }});
        }

        .service-bg-bottom {
            position: absolute;
            right: 0px;
            bottom: 0px;
            z-index: -1;
            width: 195px;
            height: 209px;
            background-repeat: no-repeat;
            background-image: url({{ asset('img/home/slogan-bg-2.png') }});
        }

        .card-img-top-service {
            width: 100%;
        }

        .card-img-top-1{
            width: 100%;
        }


        .more-services-1 .card {
            border: 0;
            padding: 20px 20px 20px 20px !important;
            border-radius: 10px;
            position: relative;
            border-radius: 10px;
            width: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
        }


        .card-body {
            z-index: 1;
            /* background: rgba(255, 255, 255, 0.9); */
            padding: 15px 30px;
            /* box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.1); */
            transition: 0.3s;
            transition: ease-in-out 0.4s;
            border-radius: 5px;
        }

        .card-img-top{
            border-radius: 10px;
        }

        .service-h2-1{
            font-size: 26px;
            font-family: 'cschatthai';
            font-weight: 600;
            /* padding: 0 15px; */
            color: #191919;
        }
        .service-h2-1-title{
            padding-top: 20px;
        }
        .service-p-1 {
            font-size: 20px;
            font-family: 'cschatthai' ;
            /* padding: 0 15px ; */
            color:#191919;
        }
        .service-p-2 {
            font-size: 20px;
            font-family: 'cschatthai' ;
            /* padding: 0 15px ; */
            color:red;
            margin-top: -15px;
        }
        .carousel-item{
            background-color: #FFF;
        }

        .card-price {
            position: absolute;
            top: 63%;
            right: 5%;

            font-family: 'cschatthaiUI' ;
            font-size: 36px;
            line-height: 25px;
        }

        .card-price-unit {
            font-family: 'cschatthaiUI' ;
            position: absolute;
            top: 70%;
            right: 6%;

            font-size: 16px;
            line-height: 25px;
        }

        .list-group-item-1{
            font-family: 'cschatthaiUI' ;
            font-size: 26px;
            line-height: 25px;
            text-align: center;
            padding-top: 18px;
            height: 60px;
            width: 100%;
            background-color: #E8E8E8;
            color: #000000;
        }

        .list-group-item-2{
            font-family: 'cschatthai' ;
            font-size: 26px;
            line-height: 25px;
            text-align: center;
            padding-top: 18px;
            height: 60px;
            width: 100%;
            background-color: #FD5A32;;
            color: #FFFFFF;
        }
        .customer-title {
            font-family: 'cschatthaiUI';
            font-size: 32px;
            color:#000000;
            /* padding-top: 60px; */
        }
        .customer{
            /* margin-top: 25px; */
            background-color: #F5F5F5;
            padding: 0px 15px;
        }

        .customer-p{
            font-family: 'cschatthai';;
            font-size: 20px;
            color: #191919;
            padding-left:30px;
            /* padding-right: 15px; */
            }
        .collapse-color{
            background-color: #DEDEDE;
        }

        hr {
                border-top: 1px solid black;
        }
    }

    @if( $title_agent == 'isDesktop')

        section {
            padding: 0 0;
        }

        .service-header{
            padding-top: 200px;
        }

        .card-img-top-service {
            width: 100%;
        }

        .service-img-bg{
            background-image: url('{{ asset('img/service/big-cover.jpg') }}');
            width: 100%;
            height: 710px;
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .service-h1{
            font-family: 'cschatthaiUI' ;
            font-size: 64px;
            line-height: 88px;
        }
        .service-p{
            font-family: 'cschatthai' ;
            font-size: 40px;
            line-height: 52px;
        }


        #more-services{
            background-color: #27222A;
            padding: 0px 15px;
        }

        .more-services-1{
            /* margin-top: 25px; */
            /* background-color: #27222A;
            padding: 0px 15px; */
        }

        .promotion-title{
            font-size: 36px;
            line-height: 50px;
            font-family: 'cschatthaiUI' ;
        }

        .slogan-bg-top-services {
            position: absolute;
            top: 2932px;
            left: 0;
        }

        .slogan-bg-bottom-services{
            position: absolute;
            right: 0px;
            top: 3413px;
            z-index: -1;
        }

        .card-shadow{
            border: 1px solid #C4C4C4;
            box-sizing: border-box;
            box-shadow: 0 10px 16px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19) !important;
            border-radius: 10px;
        }

        .card-title{
            font-family:'cschatthaiUI';
            font-size: 20px;
            line-height: 23px;
            font-weight: bold;
            text-align: center;
            padding-top: 10px;
            color:#191919;
        }

        .card-p{
            font-size: 16px;
            line-height: 25px;
        }

        .btn-service{
            background: #FB872D;
            border-radius: 8px;
            font-size: 20px;
            color: #FFF;
            font-family:'cschatthaiUI';
            width: 100%;
        }

        .service-bg-top {
            position: absolute;
            left: 0px;
            top: 0px;
            z-index: 0;
            width: 356px;
            height: 379px;
            background-repeat: no-repeat;
            background-image: url({{ asset('img/service/service-bg-top.svg') }});
        }

        .service-bg-bottom {
            position: absolute;
            right: 0px;
            bottom: 0px;
            z-index: 2;
            width: 356px;
            height: 379px;
            background-repeat: no-repeat;
            background-image: url({{ asset('img/service/service-bg-bottom.svg') }});
        }

        .card-img-top-service {
            width: 100%;
        }

        .card-img-top-1{
            width: 100%;
        }

        .card {
            border: 0;
            padding: 20px 20px 20px 20px !important;
            border-radius: 10px;
            position: relative;
            border-radius: 10px;
            width: 100%;
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center center;
        }

        .card-body {
            z-index: 10;
            /* background: rgba(255, 255, 255, 0.9); */
            padding: 15px 30px;
            /* box-shadow: 0px 2px 15px rgba(0, 0, 0, 0.1); */
            transition: 0.3s;
            transition: ease-in-out 0.4s;
            border-radius: 5px;
        }

        .card-img-top{
            border-radius: 10px;
        }

        .service-title{
            font-family:'cschatthaiUI';
            font-size:36px;
            color:#FFFFFF;
        }

        .service-h2-1{
            font-size: 26px;
            font-family: 'cschatthai';
            font-weight: 600;
            /* padding: 0 15px; */
            color: #191919;
        }
        .service-h2-1-title{
            padding-top: 20px;
        }
        .service-p-1 {
            font-size: 20px;
            font-family: 'cschatthai' ;
            /* padding: 0 15px ; */
            color:#191919;
        }
        .service-p-2 {
            font-size: 20px;
            font-family: 'cschatthai' ;
            /* padding: 0 15px ; */
            color:red;
            margin-top: -15px;
        }

        .customer-title{
            font-size: 36px;
            line-height: 50px;
            font-family: 'cschatthaiUI' ;
        }

        .customer-p{
            font-size: 20px;
            line-height: 30px;
            font-family: 'cschatthai' ;
        }

        /* Card Flip */

        .flip-card {
            background-color: transparent;
            width: 330px;
            height: 500px;
            perspective: 1000px;
        }

        .flip-card-inner {
            position: relative;
            width: 100%;
            height: 100%;
            text-align: center;
            transition: transform 0.6s;
            transform-style: preserve-3d;
            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
        }

        .flip-card:hover .flip-card-inner {
            transform: rotateY(180deg);
        }
        .flip-card-front, .flip-card-back {
            position: absolute;
            width: 100%;
            height: 100%;
            -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
        }

        .flip-card-front {
            background-color: white;
            padding: 20px;
            border-radius: 10px;
            text-align: left;
            /* position: absolute;
            left: 0px;
            top: 0px;
            z-index: 4000; */

        }

        .flip-card-back {
            background-color: white;
            /* color: white; */
            transform: rotateY(180deg);
            padding: 20px;
            border-radius: 10px;
            text-align: left;
        }

        .service-hover{
            z-index: 1;
            position: absolute;
            width: 100%;
            height:100%;
            background-color: rgb(252, 144, 11, 0.7);
            top: 0;
            left: 0;
            border-radius: 10px;

        }

        .btn-flip{
            z-index: 2;
            opacity: 1 !important;
            position: fixed;
            bottom: 30px;
            left: 35%;
            font-family: 'cschatthai';
            font-size: 20px;
            background-color: #FB872D;
            color:white;
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
            border-radius: 8px;
        }

        .content{
            position: relative;
            z-index: 100;

        }

        #main{
            position: relative;
            left: 0;
            top: -5px;
        }

        /* End Card Flip */

    @endif
</style>

@endsection

@section('jquery')
        $('#collapseOne.collapse').on('shown.bs.collapse', function() {
            $(this).parent().find('.fa-plus').removeClass('fa-minus').addClass('fa-minus');
        }).on('hidden.bs.collapse', function() {
            $(this).parent().find('.fa-minus').removeClass('fa-minus').addClass('fa-plus');
        });
@endsection


@section('content')


    <div id="service" class="service section-bg">
        <div class="container-fluid px-0">
            @if( $title_agent == 'isDesktop')
                <div class="service-img-bg">
                    <div class="container">
                        <div class="service-header text-white">
                            <h3 class="service-h1" data-aos="fade-up" >บริการ</h3>
                            <p class="service-p" data-aos="fade-up" >ธุรกิจของไพวอทฯ นอกจากจะให้บริการพนักงานรับส่งเอกสารแล้ว เรายังมีบริการ อื่นๆและระบบการจัดการที่ครอบคลุมสู่การเป็นผู้ให้บริการด้านรับส่งเอกสารแบบครบวงจร โดยเน้นเรื่องความเชี่ยวชาญ ความปลอดภัยและเทคโนโลยี เพื่อส่งมอบ บริการที่มีคุณภาพและสร้างความพึงพอใจอย่างเหนือความคาดหมายให้แก่ลูกค้าของเรา</p>
                        </div>
                    </div>

                </div>
            @endif
            @if( $title_agent == 'isMobile')
                <div class="service-header service-img-bg text-white">
                    <h3 class="service-h1">บริการ</h3>
                    <p class="service-p px-0">ธุรกิจของไพวอทฯ นอกจากจะให้บริการพนักงานรับส่งเอกสารแล้ว เรายังมีบริการ อื่นๆและระบบการจัดการที่ครอบคลุมสู่การเป็นผู้ให้บริการด้านรับส่งเอกสารแบบครบวงจร โดยเน้นเรื่องความเชี่ยวชาญ ความปลอดภัยและเทคโนโลยี เพื่อส่งมอบ บริการที่มีคุณภาพและสร้างความพึงพอใจอย่างเหนือความคาดหมายให้แก่ลูกค้าของเรา</p>
                </div>
            @endif
        </div>
    </div>

    <main id="main">

        @if( $title_agent == 'isDesktop')
            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="service-bg-top"></div>
                    </div>
                </div>
            </div>
        @endif


        <div id="more-services" class="more-services-1">

            <div class="container py-5">

                @if( $result == 'isTablet')
                    <h2 class="service-title content py-4 text-center">บริการต่างๆ</h2>
                @else
                    <h2 class="service-title content py-4 ">บริการต่างๆ</h2>
                @endif



                @if( $title_agent == 'isDesktop')
                    <div class="row content text-white">
                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <img src="{{ asset('img/service/service-01.jpg') }}" class="card-img-top">
                                        <h3 class="service-h2-1 service-h2-1-title">แมสเซ็นเจอร์แบบรายเดือน</h3>
                                        <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร รับเช็ค วางบิล
                                            เอกสารสำคัญประจำสำนักงานของท่าน <br>
                                            อัตราเริ่มต้นที่ 16,500 บาท/เดือน</p>
                                    </div>
                                    <div class="flip-card-back">
                                        <div class="service-hover">
                                            <a class="btn  btn-flip" href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank" >คลิกเพื่อสมัคร</a>
                                        </div>
                                        <img src="{{ asset('img/service/service-01.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title">แมสเซ็นเจอร์แบบรายเดือน</h3>
                                        <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร รับเช็ค วางบิล
                                                    เอกสารสำคัญประจำสำนักงานของท่าน <br>
                                                    อัตราเริ่มต้นที่ 16,500 บาท/เดือน</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <img src="{{ asset('img/service/service-02.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายวัน</h3>
                                        <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร
                                            รับเช็ค วางบิลเอกสารสำคัญ<br>
                                            อัตราเริ่มต้นที่ 800 บาท/วัน</p>
                                    </div>
                                    <div class="flip-card-back">
                                        <div class="service-hover">
                                            <a class="btn  btn-flip" href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank" >คลิกเพื่อสมัคร</a>
                                        </div>
                                        <img src="{{ asset('img/service/service-02.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายวัน</h3>
                                        <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร
                                            รับเช็ค วางบิลเอกสารสำคัญ<br>
                                            อัตราเริ่มต้นที่ 800 บาท/วัน</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <img src="{{ asset('img/service/service-03.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบเร่งด่วน</h3>
                                        <p class="service-p-1">บริการรับส่งเอกสารแบบงานด่วน
                                            พัสดุที่ต้องการส่งแบบทันที
                                            อัตราเริ่มต้นที่ 250 บาท<br></p>
                                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                    </div>
                                    <div class="flip-card-back">
                                        <div class="service-hover">
                                            <a class="btn  btn-flip" href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank" >คลิกเพื่อสมัคร</a>
                                        </div>
                                        <img src="{{ asset('img/service/service-03.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบเร่งด่วน</h3>
                                        <p class="service-p-1">บริการรับส่งเอกสารแบบงานด่วน
                                            พัสดุที่ต้องการส่งแบบทันที
                                            อัตราเริ่มต้นที่ 250 บาท<br></p>
                                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <img src="{{ asset('img/service/service-04.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายชิ้น</h3>
                                        <p class="service-p-1">บริการรับส่งเอกสาร (สินค้า)
                                            แบบรายชิ้น <br>อัตราเริ่มต้นที่ 95 บาท <br></p>
                                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                    </div>
                                    <div class="flip-card-back">
                                        <div class="service-hover">
                                            <a class="btn  btn-flip" href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank" >คลิกเพื่อสมัคร</a>
                                        </div>
                                        <img src="{{ asset('img/service/service-04.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายชิ้น</h3>
                                        <p class="service-p-1">บริการรับส่งเอกสาร (สินค้า)
                                            แบบรายชิ้น <br>อัตราเริ่มต้นที่ 95 บาท <br></p>
                                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <img src="{{ asset('img/service/service-05.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">จัดส่งสินค้าโดยรถยนต์</h3>
                                        <p class="service-p-1">บริการจัดส่งสินค้าโดยรถยนต์ในกรุงเทพและต่างจังหวัด
                                            ราคาเริ่มต้นที่ 1,000 บาท<br></p>
                                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                    </div>
                                    <div class="flip-card-back">
                                        <div class="service-hover">
                                            <a class="btn  btn-flip" href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank" >คลิกเพื่อสมัคร</a>
                                        </div>
                                        <img src="{{ asset('img/service/service-05.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">จัดส่งสินค้าโดยรถยนต์</h3>
                                        <p class="service-p-1">บริการจัดส่งสินค้าโดยรถยนต์ในกรุงเทพและต่างจังหวัด
                                            ราคาเริ่มต้นที่ 1,000 บาท<br></p>
                                        <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4 mb-5" data-aos="fade-up">
                            <div class="flip-card">
                                <div class="flip-card-inner">
                                    <div class="flip-card-front">
                                        <img src="{{ asset('img/service/service-06.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">บริการจัดการคลังสินค้า</h3>
                                        <p class="service-p-1">งานบริการเช่าคลังสินค้าและบริการจัดการ
                                            คลังสินค้าจัดเก็บสินค้าพรีเมี่ยมโดยมีระบบ
                                            รองรับดูแลตลอดเวลา 24 ชม.อัตราเริ่มต้นที่
                                            200 / ตรม</p>
                                    </div>
                                    <div class="flip-card-back">
                                        <div class="service-hover">
                                            <a class="btn  btn-flip" href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform" target="_blank" >คลิกเพื่อสมัคร</a>
                                        </div>
                                        <img src="{{ asset('img/service/service-06.jpg') }}" class="card-img-top" alt="">
                                        <h3 class="service-h2-1 service-h2-1-title ">บริการจัดการคลังสินค้า</h3>
                                        <p class="service-p-1">งานบริการเช่าคลังสินค้าและบริการจัดการ
                                            คลังสินค้าจัดเก็บสินค้าพรีเมี่ยมโดยมีระบบ
                                            รองรับดูแลตลอดเวลา 24 ชม.อัตราเริ่มต้นที่
                                            200 / ตรม</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                @endif
                @if( $title_agent == 'isMobile')
                    <div class="row content text-white">
                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="portfolio-wrap">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank">
                                <div class="card">
                                    <img src="{{ asset('img/service/service-01.jpg') }}" class="card-img-top">
                                    <h3 class="service-h2-1 service-h2-1-title text-left">แมสเซ็นเจอร์แบบรายเดือน</h3>
                                    <p class="service-p-1 text-left">งานบริการพนักงานรับส่งเอกสาร รับเช็ค วางบิล
                                        เอกสารสำคัญประจำสำนักงานของท่าน <br>
                                        อัตราเริ่มต้นที่ 16,500 บาท/เดือน</p>
                                </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="portfolio-wrap">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank">
                                <div class="card" style="">
                                    <img src="{{ asset('img/service/service-02.jpg') }}" class="card-img-top" alt="">
                                    <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายวัน</h3>
                                    <p class="service-p-1">งานบริการพนักงานรับส่งเอกสาร
                                        รับเช็ค วางบิลเอกสารสำคัญ<br>
                                        อัตราเริ่มต้นที่ 800 บาท/วัน</p>
                                </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="portfolio-wrap">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank">
                                <div class="card" style="">
                                    <img src="{{ asset('img/service/service-03.jpg') }}" class="card-img-top" alt="">
                                    <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบเร่งด่วน</h3>
                                    <p class="service-p-1">บริการรับส่งเอกสารแบบงานด่วน
                                        พัสดุที่ต้องการส่งแบบทันที
                                        อัตราเริ่มต้นที่ 250 บาท<br></p>
                                    <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="portfolio-wrap">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank">
                                <div class="card" style="">
                                    <img src="{{ asset('img/service/service-04.jpg') }}" class="card-img-top" alt="">
                                    <h3 class="service-h2-1 service-h2-1-title ">แมสเซ็นเจอร์แบบรายชิ้น</h3>
                                    <p class="service-p-1">บริการรับส่งเอกสาร (สินค้า)
                                        แบบรายชิ้น <br>อัตราเริ่มต้นที่ 95 บาท <br></p>
                                    <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4" data-aos="fade-up">
                            <div class="portfolio-wrap">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank">
                                <div class="card" style="">
                                    <img src="{{ asset('img/service/service-05.jpg') }}" class="card-img-top" alt="">
                                    <h3 class="service-h2-1 service-h2-1-title ">จัดส่งสินค้าโดยรถยนต์</h3>
                                    <p class="service-p-1">บริการจัดส่งสินค้าโดยรถยนต์ในกรุงเทพและต่างจังหวัด
                                        ราคาเริ่มต้นที่ 1,000 บาท<br></p>
                                    <p class="service-p-2">*** สงวนสิทธิ์เฉพาะลูกค้าเดิมเท่านั้น</p>
                                </div>
                                </a>
                            </div>
                        </div>

                        <div class="col-lg-4 col-md-6 mt-4 mb-5" data-aos="fade-up">
                            <div class="portfolio-wrap">
                                <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank">
                                <div class="card" style="">
                                    <img src="{{ asset('img/service/service-06.jpg') }}" class="card-img-top" alt="">
                                    <h3 class="service-h2-1 service-h2-1-title ">บริการจัดการคลังสินค้า</h3>
                                    <p class="service-p-1">งานบริการเช่าคลังสินค้า และ บริการจัดการ
                                        คลังสินค้า เช่น จัดเก็บสินค้าพรีเมี่ยม โดยมีระบบ
                                        รองรับดูแลตลอดเวลา 24 ชม. อัตราเริ่มต้นที่
                                        200 / ตรม</p>
                                </div>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif


            </div>

        </div>

        @if( $title_agent == 'isDesktop')

            <div class="container-fluid">
                <div class="row">
                    <div class="col">
                        <div class="service-bg-bottom"></div>
                    </div>
                </div>
            </div>

        @endif

        {{-- <div id="portfolio" class="portfolio">

            <div class="container-fluid">
                    <p class="promotion-title ml-4 mt-3">โปรโมชั่น</p>
            </div>

            <div class="container">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">

                    <div class="card card-shadow mb-5">
                        <div class="carousel-inner">

                            <div class="carousel-item active">
                                <img class="card-img-top-1" src="{{ asset('img/service/Promotion1.jpg') }}" alt="แพ็กเกจส่งสินค้าเร่งด่วน">
                                <div class="card-body">
                                    <h5 class="card-title">แมสเซ็นเจอร์รายเดือนราคาเริ่มต้น</h5>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item-1">16,500 บาท/เดือน</li>
                                    <li class="list-group-item-2">แถมฟรีบริการแมสเซ็นเจอร์รายวัน 5 วัน</li>
                                </ul>
                                <div class="text-center p-3">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank" class="btn btn-service">สมัครใช้งาน</a>
                                </div>
                            </div>

                            <div class="carousel-item">
                                <img class="card-img-top-1" src="{{ asset('img/service/Promotion1.png') }}" alt="แพ็กเกจส่งสินค้าเร่งด่วน">
                                <div class="card-body">
                                    <h5 class="card-title">แมสเซ็นเจอร์รายเดือนราคาเริ่มต้น</h5>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item-1">16,500 บาท/เดือน</li>
                                    <li class="list-group-item-2">แถมฟรีบริการแมสเซ็นเจอร์รายวัน 5 วัน</li>
                                </ul>
                                <div class="text-center p-3">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank" class="btn btn-service">สมัครใช้งาน</a>
                                </div>
                            </div>

                            <div class="carousel-item">
                                <img class="card-img-top-1" src="{{ asset('img/service/Promotion1.png') }}" alt="แพ็กเกจส่งสินค้าเร่งด่วน">
                                <div class="card-body">
                                    <h5 class="card-title">แมสเซ็นเจอร์รายเดือนราคาเริ่มต้น</h5>
                                </div>
                                <ul class="list-group list-group-flush">
                                    <li class="list-group-item-1">16,500 บาท/เดือน</li>
                                    <li class="list-group-item-2">แถมฟรีบริการแมสเซ็นเจอร์รายวัน 5 วัน</li>
                                </ul>
                                <div class="text-center p-3">
                                    <a href="https://docs.google.com/forms/d/e/1FAIpQLSesUlEqmqjmBLAqwKJPoN3niSdAhnpT5Fx-vgfUJXd4A8qx3w/viewform"target="_blank" class="btn btn-service">สมัครใช้งาน</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>

                    <ol class="carousel-indicators">
                        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active dot"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="1" class="dot"></li>
                        <li data-target="#carouselExampleIndicators" data-slide-to="2" class="dot"></li>
                    </ol>


                </div>


                <img src="{{ asset('img/home/slogan-bg-2.png') }}" class="slogan-bg-bottom-services">
            </div>
        </div> --}}

        @if( $title_agent == 'isDesktop')

        <div  class="customer">
                <div class="container">
                    <div class="container my-5 py-3">
                        <div>
                            <h2 class="customer-title py-4" data-aos="fade-up">ลูกค้าของเรา</h2>
                        </div>
                        <div class="row" data-aos="fade-up">
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">ธนาคารทหารไทย จำกัด (มหาชน)</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท เจเนอรัล เอาท์ซอร์สซิ่ง จำกัด</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท ซาแวนท์เอ็นจิเนียริ่ง จำกัด </p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท ข้อมูลเครดิตแห่งชาติ จำกัด</p>
                            </div>
                        </div>

                        <div class="row" data-aos="fade-up">
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">ธนาคารออมสิน</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท ธนาคารกสิกรไทย จำกัด (มหาชน)</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท ไอโอ โซลูชั่น จำกัด </p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">ธนาคารแลนด์ แอนด์ เฮ้าส์ จำกัด</p>
                            </div>
                        </div>

                        <div class="row" data-aos="fade-up">
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">ธนาคารแห่งประเทศไทย</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">DHL Distribution (Thailand) Ltd.</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท เงินติดล้อ จำกัด</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท เจ ฟินเทค จำกัด</p>
                            </div>
                        </div>

                        <div class="row" data-aos="fade-up">
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">INGRAM MICRO (THAILAND) CO.,LTD</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท คลีน พลัส (ประเทศไทย) จำกัด</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">บริษัท สยามโตโยต้าอุตสาหกรรม จำกัด</p>
                            </div>
                            <div class="col-sm-6 col-lg-4">
                                <p class="customer-p">ห้างหุ้นส่วนจำกัดอนันตะ เทรดดิ้ง</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif


        @if( $title_agent == 'isMobile')

            <div  class="customer">
                <div class="container">
                    <hr />
                    <div id="accordion">
                        <div id="headingOne">
                            <div class="d-flex">
                                <div class="w-100">
                                    <p class="customer-title mb-0 ">ลูกค้าของเรา</p>
                                </div>

                                <div class="accordion flex-shrink-1 align-self-center " id="accordionExample">
                                    <button class="btn btn-collapse" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        <i class="fas fa-plus"></i>
                                    </button>
                                </div>
                            </div>
                            <div id="collapseOne" class="collapse hide" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="collapse-color">
                                    <div class="d-flex flex-wrap">
                                        <p class="customer-p flex-fill w-100 m-0 mt-3" data-aos="fade-up">ธนาคารทหารไทย จำกัด (มหาชน)</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">ธนาคารออมสิน</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">ธนาคารแห่งประเทศไทย</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">INGRAM MICRO (THAILAND) CO.,LTD.</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท เจเนอรัล เอาท์ซอร์สซิ่ง จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท ธนาคารกสิกรไทย จำกัด (มหาชน)</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">DHL Distribution (Thailand) Ltd.</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท คลีน พลัส (ประเทศไทย) จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท ซาแวนท์เอ็นจิเนียริ่ง จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท ไอโอ โซลูชั่น จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท เงินติดล้อ จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท สยามโตโยต้าอุตสาหกรรม จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท ข้อมูลเครดิตแห่งชาติ จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">ธนาคารแลนด์ แอนด์ เฮ้าส์ จำกัด (มหาชน)</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">บริษัท เจ ฟินเทค จำกัด</p>
                                        <p class="customer-p flex-fill w-100 m-0" data-aos="fade-up">ห้างหุ้นส่วนจำกัดอนันตะ เทรดดิ้ง</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                     <hr />
                </div>
            </div>

        @endif


    </br>
@endsection
