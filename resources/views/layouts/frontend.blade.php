<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="pivot" name="description">
  <meta content="pivot" name="keywords">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {{-- <title>{{ config('app.name')}} @yield('title-agent')</title> --}}
  <title>@yield('title-agent')</title>
 <!-- Cookie Consent by https://www.cookiewow.com --> <script type="text/javascript" src="https://cookiecdn.com/cwc.js"></script> <script id="cookieWow" type="text/javascript" src="https://cookiecdn.com/configs/Q7S2wsV8WF5Ek1DUJsxWK1DZ" data-cwcid="Q7S2wsV8WF5Ek1DUJsxWK1DZ"></script>
  <!-- Favicons -->
  <link href="{{ asset('img/favicon.png')}}" rel="icon">
  <link href="{{ asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css')}}" rel="stylesheet">

  <!-- Vendor CSS Files -->
  {{-- <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --}}
  <link href="{{ asset('css/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('css/aos.css') }}" rel="stylesheet">

  <link href="{{ asset('css/fontawesome/css/all.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fontawesome/css/fontawesome.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fontawesome/css/brands.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fontawesome/css/solid.css') }}" rel="stylesheet">
  <link rel="icon" href={{ asset('img/logo-pv1.svg') }} type="image/icon type">

  <!-- Template Main CSS File -->
  <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5FGS6DD');</script>
 <!-- End Google Tag Manager -->

 @yield('header-script')
 <style>
    @font-face {
        font-family: 'cschatthai';
        src: url('{{ asset('css/fonts/cschatthai/CSChatThai.ttf') }}') format('truetype');
    }

    @font-face {
        font-family: 'cschatthaiUI';
        src: url('{{ asset('css/fonts/cschatthai/CSChatThaiUI.ttf') }}') format('truetype');
    }

     @media (max-width: 768px) {

        * {
            font-family: 'cschatthai' ;
        }

        .container-fluid{
            padding: 0px;
        }

        #header{
            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25) !important;
        }

        .header-logo{
            position: absolute;
            width: 76px;
            height: 76px;
            left: 0px;
            top: 0px;

            margin-left: 2rem;

            box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25)
        }

        .logo{
            margin-left: 7rem;
        }

        .owl-dots{
            position: relative;
            top: -50px;
        }

        .home-cover-img{
            width: 375px;
            height: 491px;
        }

        .portfolio-container{
            margin-top: 40px;
        }

        .slogan-bg-top{
            position: absolute;
            top: 550px;
            left: 0;
        }

        .slogan-bg-bottom{
            position: absolute;
            right: 0px;
            bottom: -25px;
            z-index: -1;
            width: 195px;
            height: 209px;
            background-image: url({{ asset('img/home/slogan-bg-2.png') }});
            background-repeat: no-repeat;
        }

        .slogan-title{
            font-size: 32px;
            font-weight: bold;

            margin-top: -28px;
            margin-left: 15px;
        }

        .slogan-img{
            box-shadow:0 10px 16px 0 rgba(0,0,0,0.2),0 4px 20px 0 rgba(0,0,0,0.19) !important;
            border-radius: 15px;
        }

        .more-services{
            margin-top: 25px;
            background-color: #27222A;
            padding: 0px 15px;
        }

        .service-title{
            font-family: 'cschatthaiUI' ;
            font-size: 32px;
            color: #fff;
            padding-top: 60px;
        }

        .service-h2 {
            font-size: 26px;
            font-family: 'cschatthai' ;
            /* padding: 0 15px ; */
        }

        .service-h2-title{
            padding-top: 30px;
        }

        .service-p {
            font-size: 20px;
            font-family: 'cschatthai' ;
            padding: 0 15px ;
        }

        .service-img-bg{
            /* 316 × 287 */
            width: 100%;
            height: 287px;
            background-image: url({{ asset('img/home/service-1.png') }});
            background-repeat: no-repeat;
            /* แก้ไขปัญหา responsive */
            /* background-size: 100% 100%; */

             /* Center and scale the image nicely */
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }

        .contact{
            margin-top: -61px;
        }

        .footer-title{
            font-family: 'cschatthai' ;
            font-size: 26px;
            color: #fff;
        }
        .footer-span{
            font-size: 16px;
            margin-top: -16px;
        }

        .footer-copyright{
            font-size: 14px;
            background-color:#292929;
        }

        .bg-footer{
            background-color: #191919;

        }

        .mobile-nav li {
            font-size: 36px;
            font-family: 'cschatthaiUI' ;
            line-height: 47px;
        }

        .footer_mobile{
            display:block !important;
        }

        .action-bar {
            position: fixed;
            bottom: 0;
            width: 100%;
            height: 60px;
            display: flex;
            -webkit-box-shadow: 0px -5px 20px -3px rgba(0,0,0,0.7);
            -moz-box-shadow: 0px -5px 20px -3px rgba(0,0,0,0.7);
            box-shadow: 0px -5px 20px -3px rgba(0,0,0,0.7);
            z-index: 2000;
        }


        .action-bar-link {
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            flex-grow: 1;
            min-width: 50px;
            overflow: hidden;
            white-space: nowrap;
            font-size: 14px;
            color: #00A6A6;
            text-decoration: none;
            -webkit-tap-highlight-color: transparent;
            transition: background-color 0.1s ease-in-out;
        }

        .action-bar-link:hover {
            /* background-color: #eeeeee; */
        }

        .action-bar-link:active {
            color: #009578;
        }

        .action-bar-text{

        }

        .mobile-nav a:hover, .mobile-nav .active > a, .mobile-nav li:hover > a {
            color: #565656 !important;
            text-decoration: none;
        }

        .active{

            background-color: #E7E7E7;
        }

        a:hover {
            color: #565656;
            text-decoration: none;
        }

    }

    .footer_mobile{
      padding-top: 5px;
      display:none;
    }

    .footer-QR-CODE{
        width: 100px;
        margin : 3px 3px 0 3px;
    }
    .div-qr-code{
        text-align: center;
    }
    .txt-rq-code{
        font-size: 14px;
    }


 </style>
 @yield('style-header')
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">

    <div class="container d-flex align-items-center">
        <img class="header-logo bg-white p-2 pb-3" src="{{ asset('img/logo-pv1.svg') }}">
      <div class="logo mr-auto">

        <h1 class="text-light"><a href="#"><span>Pivot Co., Ltd</span></a></h1>
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="{{ request()->is('/') ? 'active' : ''  }}"><a href="{{ route('home') }}">หน้าแรก</a></li>
          <li class="{{ request()->is('service') ? 'active' : ''  }}"><a href="{{ route('service') }}">บริการ</a></li>
          <li class="{{ request()->is('about') ? 'active' : ''  }}"><a href="{{ route('about') }}">เกี่ยวกับเรา</a></li>
          <li class="{{ request()->is('career') ? 'active' : ''  }}"><a href="{{ route('career') }}">สมัครร่วมทีม</a></li>
          <li class="{{ request()->is('activity') ? 'active' : ''  }}"><a href="{{ route('activity') }}">กิจกรรม</a></li>
          <li class="{{ request()->is('contact') ? 'active' : ''  }}"><a href="{{ route('contact') }}">ติดต่อเรา</a></li>
        </ul>
      </nav>
    </div>
  </header>

    @yield('content')
         <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
      <div class="container">

      <!-- Grid row -->
      <div class="row bg-footer text-white">

        <!-- Grid column -->
        <div class="col-lg-3 col-md-6 mt-4 mt-md-0 aos-init aos-animate">

          <!-- Content -->
          <p class="footer-title">บริษัท ไพวอท จำกัด( สำนักงานใหญ่ )</p>
          <p class="footer-span">1000/67-74 อาคารลิเบอร์ตี้ พลาซ่า ชั้น 1-3 ซอย สุขุมวิท 55 (ซอยทองหล่อ) แขวงคลองตันเหนือเขต วัฒนา กทม. 10110</p>

        </div>
        <!-- Grid column -->

         <!--<hr class="clearfix w-100 d-md-none pb-3">-->

        <!-- Grid column -->
        <div class="col-lg-3 col-md-6 mt-4 mt-md-0 aos-init aos-animate">

          <!-- Links -->
          <h5 class="footer-title">ติดต่อ</h5>
            <p class="footer-span">Call Center : 02-391-3344</p>
         <!--    <p class="footer-span">Marketing : 061-823-4284 (คุณเมย์)</p> -->
            {{-- <p class="footer-span">ฝ่ายขายและการตลาด : 061-823-4284</p> --}}
            <p class="footer-span">ฝ่ายขายและการตลาด : 099-053-3999</p>
            <p class="footer-span">ฝ่ายบุคคล : 065-940-1127</p>
            <p class="footer-span">สมัครงาน : 061-823-4314</p>
            <p class="footer-span">Fax : 02-381-9761</p>

        </div>
        <div class="col-lg-3 col-md-6 mt-4 mt-md-0 aos-init aos-animate">

          <h5 class="footer-title">EMAIL</h5>
            <p class="footer-span">services@pivot.co.th</p>
            <p class="footer-span">sales@pivot.co.th (ฝ่ายขายและการตลาด)</p>
            <!-- <p class="footer-span">thitima.au@pivot.co.th (ฝ่ายการตลาด)</p> -->
            <p class="footer-span">recruit@pivot.co.th (ฝ่ายบุคคล)</p>

        </div>
        <div class="col-lg-3 col-md-6 mt-4 mt-md-0 aos-init aos-animate">
            <div class="d-flex justify-content-around">
                <div class="p-2 div-qr-code">
                    <img class="footer-QR-CODE" src="{{ asset('img_desktop/footer/QR-CODE_MK.png') }}" class="footer-logo-line" alt="ไม่มีรูป">
                    <p class="txt-rq-code">(ฝ่ายขายและการตลาด)</p>
                </div>
                <div class="p-2 div-qr-code">
                    <img class="footer-QR-CODE" src="{{ asset('img_desktop/footer/QR-CODE.jpg') }}" class="footer-logo-line" alt="ไม่มีรูป">
                    <p class="txt-rq-code">(สมัครงาน)</p>
                </div>
            </div>
      </div>
        <!-- Grid column -->

      </div>
      <!-- Grid row -->
      </div>
    </section>

  </main>

    <footer class="page-footer pb-5">

        <div class="footer-copyright text-center text-white  py-4">© All Rights Reserved. {{ date('Y')}} Pivot Co., Ltd.</div>

    </footer>

    <nav class="action-bar footer_mobile bg-white">
        <div class="row">
          <div class="col-4">
            <a href="tel:0618234284" class="action-bar-link">
            <i class="fas fa-phone-alt fa-2x"></i>
              <span class="action-bar-text">PHONE</span>
            </a>
          </div>
          <div class="col-4">
            <a href="https://goo.gl/maps/rt45MHFqbwNd5s9E6" class="action-bar-link">
            <i class="fas fa-map-marker-alt fa-2x"></i>
              <span class="action-bar-text">MAP</span>
          </span>
            </a>
          </div>
          <div class="col-4">
            <a href="https://lin.ee/S0jeGNn" class="action-bar-link">
              <i class="fab fa-line fa-2x"></i>
              <span class="action-bar-text">LINE</span>
            </a>
        </div>
    </nav>


  {{-- <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a> --}}

  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ ('js/jquery.easing.min.js') }}"></script>
  <script src="{{ ('js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ ('js/counterup.min.js') }}"></script>
  <script src="{{ ('js/owl.carousel.min.js') }}"></script>
  <script src="{{ ('js/isotope.pkgd.min.js') }}"></script>
  <script src="{{ ('js/venobox.min.js') }}"></script>
  <script src="{{ ('js/aos.js') }}"></script>
  <script src="{{ ('js/main.js') }}"></script>
  <script src="{{ ('css/fontawesome/js/all.js') }}"></script>

  <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FGS6DD"
     height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
   <!-- End Google Tag Manager (noscript) -->

  @yield('footer-script')

  <script>
    $(document).ready(function() {
         @yield('jquery')
    });
  </script>
</body>

</html>
