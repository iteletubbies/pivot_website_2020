<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="pivot" name="description">
  <meta content="pivot" name="keywords">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {{-- <title>{{ config('app.name')}}</title> --}}
  <title>@yield('title-agent')</title>
 <!-- Cookie Consent by https://www.cookiewow.com --> <script type="text/javascript" src="https://cookiecdn.com/cwc.js"></script> <script id="cookieWow" type="text/javascript" src="https://cookiecdn.com/configs/Q7S2wsV8WF5Ek1DUJsxWK1DZ" data-cwcid="Q7S2wsV8WF5Ek1DUJsxWK1DZ"></script>

  <!-- Favicons -->
  <link href="{{ asset('img/favicon.png')}}" rel="icon">
  <link href="{{ asset('img/apple-touch-icon.png')}}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css')}}" rel="stylesheet">

  <!-- Vendor CSS Files -->
  {{-- <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> --}}
  <link href="{{ asset('css/icofont.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/remixicon.css') }}" rel="stylesheet">
  <link href="{{ asset('css/boxicons.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/owl.carousel.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/venobox.css') }}" rel="stylesheet">
  <link href="{{ asset('css/aos.css') }}" rel="stylesheet">

  <link href="{{ asset('css/fontawesome/css/all.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fontawesome/css/fontawesome.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fontawesome/css/brands.css') }}" rel="stylesheet">
  <link href="{{ asset('css/fontawesome/css/solid.css') }}" rel="stylesheet">
  <link rel="icon" href={{ asset('img/logo-pv1.svg') }} type="image/icon type">

  <!-- Template Main CSS File -->
  <link href="{{ asset('css/style_desktop.css') }}" rel="stylesheet">
  <!-- Google Tag Manager -->
  <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-5FGS6DD');</script>
  <!-- End Google Tag Manager -->

 @yield('header-script')
 <style>

 </style>
 @yield('style-header')
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center">

    <div class="container d-flex align-items-center">

    <div class="logo-block align-middle">
        <div>
            <img class="header-logo bg-white p-2 pb-3" src="{{ asset('img/Pivotlogo.svg') }}">
        </div>
        <div class="logo">
            <h1 class=""><a href="#"><span>&nbsp; Pivot Co., Ltd</span></a></h1>
        </div>
    </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
          <li class="{{ request()->is('/') ? 'active' : ''  }}"><a href="{{ route('home') }}">หน้าแรก</a></li>
          <li class="{{ request()->is('service') ? 'active' : ''  }}"><a href="{{ route('service') }}">บริการ</a></li>
          <li class="{{ request()->is('about') ? 'active' : ''  }}"><a href="{{ route('about') }}">เกี่ยวกับเรา</a></li>
          <li class="{{ request()->is('career') ? 'active' : ''  }}"><a href="{{ route('career') }}">สมัครร่วมทีม</a></li>
          <li class="{{ request()->is('activity') ? 'active' : ''  }}"><a href="{{ route('activity') }}">กิจกรรม</a></li>
          <li class="{{ request()->is('contact') ? 'active' : ''  }}"><a href="{{ route('contact') }}">ติดต่อเรา</a></li>
        </ul>
      </nav>
    </div>
  </header>
    @yield('content')

    <footer id="footer">

        <div class="footer-top">
          <div class="container">
            <div class="row">

              <div class="col-lg-3 col-md-6 footer-contact">
                <h3>บริษัท ไพวอท จำกัด
               <br>(สำนักงานใหญ่)<span></span></h3>
                <p>1000/67-74 อาคารลิเบอร์ตี้ พลาซ่า ชั้น 1-3 ซอย สุขุมวิท 55 (ซอยทองหล่อ) แขวงคลองตันเหนือเขต วัฒนา กทม. 10110</p>
              </div>

              <div class="col-lg-3 col-md-6 footer-contact">
                <h3>ติดต่อเรา</h3>
                  <a href="#">Call Center : 02-391-3344</a><br>
                  <a href="#">ฝ่ายขายและการตลาด : 099-053-3999</a><br>
                  {{-- <a href="#">ฝ่ายขายและการตลาด : 061-823-4284</a><br> --}}
                <!--   <a href="#">Marketing : 061-823-4284 (คุณเมย์)</a><br> -->
                  <a href="#">ฝ่ายบุคคล : 065-940-1127</a><br>
                  <a href="#">สมัครงาน : 061-823-4314</a><br>
                  <a href="#">Fax : 02-381-9761</a>
              </div>

              <div class="col-lg-3 col-md-6 footer-contact">
                <h3>E-mail</h3>

                <a href="#">services@pivot.co.th</a><br>
                <a href="#">sales@pivot.co.th (ฝ่ายขายและการตลาด)</a><br>
       <!--          <a href="#">thitima.au@pivot.co.th (ฝ่ายการตลาด)</a><br> -->
                <a href="#">recruit@pivot.co.th (ฝ่ายบุคคล)</a><br>

              </div>

              <div class="col-lg-3 col-md-6 footer-contact">
                    <div class="d-flex justify-content-around">
                        <div class="p-2 div-qr-code">
                            <img class="footer-QR-CODE" src="{{ asset('img_desktop/footer/QR-CODE_MK.png') }}" class="footer-logo-line" alt="ไม่มีรูป">
                            <p class="txt-rq-code">(ฝ่ายขายและการตลาด)</p>
                        </div>
                        <div class="p-2 div-qr-code">
                            <img class="footer-QR-CODE" src="{{ asset('img_desktop/footer/QR-CODE.jpg') }}" class="footer-logo-line" alt="ไม่มีรูป">
                            <p class="txt-rq-code">(สมัครงาน)</p>
                        </div>
                    </div>
              </div>
              <!-- <div class="col-lg-2 col-md-6 footer-links">
                <img class="footer-QR-CODE" src="{{ asset('img_desktop/footer/QR-CODE.jpg') }}" class="footer-logo-line" alt="ไม่มีรูป">
              </div> -->

            </div>
          </div>
        </div>

        <div class="container py-4">

          <div class="text-center d-md-flex justify-content-between">
            <p class="mb-0">&copy; {{ date('Y')}} Pivot Co., Ltd. All Rights Reserved.</p>
            <a href="{{ route('privacy-notice') }}">นโยบายความเป็นส่วนตัว</a>
          </div>

        </div>

    </footer>


  </main>



  {{-- <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a> --}}

  <script src="{{ asset('js/app.js') }}"></script>
  <script src="{{ ('js/jquery.easing.min.js') }}"></script>
  <script src="{{ ('js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ ('js/counterup.min.js') }}"></script>
  <script src="{{ ('js/owl.carousel.min.js') }}"></script>
  <script src="{{ ('js/isotope.pkgd.min.js') }}"></script>
  <script src="{{ ('js/venobox.min.js') }}"></script>
  <script src="{{ ('js/aos.js') }}"></script>
  <script src="{{ ('js/js_desktop.js') }}"></script>
  <script src="{{ ('css/fontawesome/js/all.js') }}"></script>

  <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5FGS6DD"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
   <!-- End Google Tag Manager (noscript) -->

  @yield('footer-script')

  <script>
    $(document).ready(function() {
         @yield('jquery')
    });
  </script>
</body>

</html>
