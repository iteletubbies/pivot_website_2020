@extends("$frontend")

@section('title-agent')
    {{-- {{ $title_agent }} --}}
    {{ config('app.name')}}
@endsection

@section('style-header')
    <style>
        @if ($title_agent != 'isDesktop')
        @media (max-width: 768px) {

            .activity-header{
                padding: 60px 30px 0 30px ;
            }

            .activity-img-bg{
                background-image: url({{ asset("img/activity/cover.png") }});

                /* Center and scale the image nicely */
                width: 100%;
                height: 487px;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
            }

            .about-h1{
                font-size: 36px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
            }

            .about-p{
                font-size: 26px;
                line-height: 34px;

                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                /* padding: 0 15px ; */
            }

            .outing{
                background-color: #fff;
            }

            .outing-h1 {
                font-size: 36px;
                font-family: 'cschatthaiUI' ;
                line-height: 50px;
            }

            .outing-cover-img {
                width: 320px;
                height: 320px;
            }

            .outing-p{
                font-family: 'cschatthaiUI' ;

                font-size: 26px;
                line-height: 36x;

                margin-top: -30px;
            }

            .outing-p-description {
                color: #191919;
                font-size: 20px;
                line-height: 26px;
                margin-top: -16px;
                padding-bottom: 15px;

            }

        }

        .outing .owl-nav, .outing .owl-dots {
            padding-top: 60px;
            text-align: center;
        }

        .outing .owl-dot {
            display: inline-block;
            margin: 0 5px;
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background-color: #ddd !important;
        }

        .outing .owl-dot.active {
            background-color: #C4C4C4 !important;
        }
        @else

        .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #f8fafc;
                background-clip: border-box;
                border: 1px solid rgba(0, 0, 0, 0);
                border-radius: 0;
            }
            .activity-header{
                padding: 60px 30px 0 30px ;
            }

            .activity-img-bg{
                width: 100%;
                height: 685px;
                background-size:cover;
                position: relative;
                background-image: url({{ asset("img_desktop/activity/cover-activity.jpg") }});
            }
            .about-h1 {
                font-size: 64px;
                font-family: 'cschatthai';
                line-height: 50px;
                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.5);
                font-weight: 500;
            }
            .about-p {
                font-family: 'cschatthai';
                font-size: 40px;
                text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                font-weight: 100;
            }
            .activity-header{
                padding-top: 10rem;
            }
            .img-activity-card{
                width:100%;
                height:100%;
                {# border-radius: 8px; #}
            }


    .activity-distance{
        padding-top:210px;
    }
    .activity-h1{
        font-family: CSchatThaiUI;
        font-size: 64px;
        color:#FFFFFF;
    }
    .activity-p-1{
        font-family: CSChatThai;
        font-size: 40px;
    }
    .activity-title{
        font-family:CSchatThaiUI;
        font-size:36px;
        color:#000000;
    }
    .activity-h4{
        font-family: CSChatThai;
        font-size: 26px;
        color:#000000;
        font-weight:bold;
        padding-top: 20px;
    }
    .activity-p{
        font-family: CSChatThai;
        font-size: 20px;
    }
    .img-activity-card{
        width:100%;
        height:100%;
        {# border-radius: 8px; #}
    }
    .modal-title{
        font-family: CSchatThaiUI;
        font-size:26px;
        color:#000000;

    }
    .modal-p{
        font-family: CSchatThai;
        font-size:20px;
        padding-left: 1rem;
        padding-right: 1rem;
        margin: 0px;
    }

     *  {
	 box-sizing: border-box;
    }











    *::before, *::after {
	 box-sizing: border-box;
    }

    .gallery {
	    padding: 0 0 4rem 0;
    }
    .img-container {
	 width: 100%;
	 height: 100%;
	 cursor: pointer;
	 overflow: hidden;
    }
    .img-container:hover .img-content-hover {
	 display: block;

    }
    .img-gallery {
	    transform: scale(1);
        transition: all 0.3s ease-in-out;
        object-fit: cover;
        width:100%;
        padding-top: 30px;
    }

    .img-container {
         width: 100%;
         height: 100%;
         cursor: pointer;
         overflow: hidden;
     }

    .img-gallery:hover {
	 transform: scale(1.03);

    }
    .img-content-hover {
	 z-index: 1;
	 position: absolute;
	 top: 0;
	 left: 0;
	 white-space: nowrap;
	 display: none;
	 padding: 1rem;
	 font-weight: 400;
	 margin-top: 1.25rem;
	 margin-left: -2rem;
    }




        @endif



    </style>

@endsection

@section('footer-script')

    <script>
    </script>

@endsection

@section('content')

    <section id="activity" class="activity">
        <div class="activity-header activity-img-bg text-white">
            <div class="container" data-aos="fade-up">
                <h3 class="about-h1">กิจกรรม</h3>
                <p class="about-p">เราส่งเสริมให้พนักงานทุกคนมีสุขภาพ
                    ร่างกายและสภาพจิตใจที่ดี โดยจัด
                    กิจกรรมสันทนาการ การแข่งขันกีฬา
                    และการท่องเที่ยว อาทิเช่น การแข่งขัน
                    ฟุตบอลประจำปี การจัดกิจกรรม CSR
                    กิจกรรม Team building และกิจกรรม
                    การท่องเที่ยวประจำปี ซึ่งถือเป็นไฮไลท์
                    สำหรับพนักงานของเราทุกๆปี</p>
            </div>
        </div>
    </section>

    {{-- <main id="main"> --}}
        @if ($title_agent != 'isDesktop')
        <section id="outing" class="outing">
            <div class="container">

                <div data-aos="fade-up">
                    <h3 class="outing-h1 pl-3 text-center">กิจกรรมที่ผ่านมา</h3>
                </div>

                <div class="owl-carousel testimonials-carousel">

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-1.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-2.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-3.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-4.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-5.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                      <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-6.jpg') }}" class="activity-cover-img" alt="">
                        </div>
                      </div>

                </div>

                <div class="row">
                    <div class="col-md-6 d-md-flex align-items-md-stretch">
                        <p class="outing-p">Pivot Outing 2019</p>
                        <p class="outing-p-description">Fun Run @ The Resort Suanpung
                            สำหรับ มกราคม 2019 ที่ผ่านมานี้ ไพวอทได้ พาเพื่อนพนักงาน ไปเยือนเมืองแห่ง
                            ไออุ่นขุนเขา ที่ เดอะรีสอร์ทสวนผึ้ง จังหวัดราชบุรี มีกิจกรรม อาทิ การบริจาคสิ่ง ของให้กับเด็กๆ พักผ่อนตามอัธยาศัย เดินถ่ายรูปเก๋ๆ หรือ เล่นสวนน้ำแบบชิคๆ
                             และ ปาร์ตี้สุดเหวี่ยงกิจกรรมยามค่ำคืน ฟินกับการวิ่งขึ้นเขายามเช้า กับกิจกรรม Pivot Funrun 2019 สามารถรับชมภาพคูลๆ ได้ในอัลบั้มเท่านั้นเลยค่ะ</p>
                    </div>
                </div>

                <div class="owl-carousel testimonials-carousel">

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                      <img src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-1.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-2.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-3.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-4.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-5.jpg') }}" class="activity-cover-img" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-6.jpg') }}" class="activity-cover-img" alt="">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 d-md-flex align-items-md-stretch">
                        <p class="outing-p">Pivot Football#2 2018</p>
                        <p class="outing-p-description">10 กันยายน 2561 กับ บรรยากาศการแข่งขันฟุตบอลกระชับความสัมพันธ์</p>
                    </div>
                </div>

                <div class="owl-carousel testimonials-carousel">

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                      <img src="{{ asset('img/activity/Pivot-Outing-2018/Outing-2018-1.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-Outing-2018/Outing-2018-2.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-Outing-2018/Outing-2018-3.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-Outing-2018/Outing-2018-4.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/activity/Pivot-Outing-2018/Outing-2018-5.jpg') }}" class="activity-cover-img" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/activity/Pivot-Outing-2018/Outing-2018-6.jpg') }}" class="activity-cover-img" alt="">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 d-md-flex align-items-md-stretch">
                        <p class="outing-p">Pivot Outing 2018</p>
                        <p class="outing-p-description">colorfull party @ The pine resort Pratumthani
                            สนุกสนานกับกิจกรรมสุดเอ็กซ์ตรีม พร้อมกับรางวัลจากเกมที่อาศัยแค่ดวงล้วนๆ</p>
                    </div>
                </div>

                <div class="owl-carousel testimonials-carousel">

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                      <img src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-1.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-2.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-3.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                      <div class="testimonial-item">
                        <img src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-4.jpg') }}" class="activity-cover-img" alt="">
                      </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-5.jpg') }}" class="activity-cover-img" alt="">
                        </div>
                    </div>

                    <div class="testimonial-wrap">
                        <div class="testimonial-item">
                          <img src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-6.jpg') }}" class="activity-cover-img" alt="">
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-6 d-md-flex align-items-md-stretch">
                        <p class="outing-p">Pivot Outing 2016</p>
                        <p class="outing-p-description">สนุกสนานกับการปลูกป่าโกงกางลดโลกร้อนที่คลองโคลน กิจกรรมเล่นเกมแข่งพายเรือ และงมหอยเพื่อมาทำอาหาร การเล่นสวนน้ำ
                            และสีสันของการพักผ่อนประจำปีนี้คือปาร์ตี้ธีม้อนยุคไปในสไตล์อโยธยา</p>
                    </div>
                </div>

            </div>
        </section>
    @else











    <div class="container">
        <h4 class="py-5 activity-title text-center">ภาพกิจกรรมที่ผ่านมา</h4>
        <div class="row">

            <div class="col-12 col-md-6 col-xs-12">
                <div class="card border-0">
                    <a data-toggle="modal" data-target=".bd1-example-modal-xl" href="#">
                        <!-- image/activity/activity-img-1.jpg  -->
                    <img class="img-activity-card" src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-1.jpg')}}"></a>
                    <div class="card-body p-0">
                        <h4 class="activity-h4">Pivot Outing 2016@ Klong klone,Samutsakorn</h4>
                        <p class="activity-p">สนุกสนานกับการปลูกป่าโกงกางลดโลกร้อนที่คลองโคลน กิจกรรมเล่นเกมแข่งพายเรือ และงมหอยเพื่อมาทำอาหาร การเล่นสวนน้ำ
                        และสีสันของการพักผ่อนประจำปีนี้คือปาร์ตี้ธีมย้อนยุคไปในสไตล์อโยธยา<br>
                        <a data-toggle="modal" data-target=".bd1-example-modal-xl" href="#" class="text-primary">เพิ่มเติม</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="modal fade bd1-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header border-0 pb-0">
                            <h4 class="modal-title">Pivot Outing 2016</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <p class="modal-p">สนุกสนานกับการปลูกป่าโกงกางลดโลกร้อนที่คลองโคลนกิจกรรมเล่นเกมแข่งพายเรือ และงมหอยเพื่อมาทำอาหาร การเล่นสวนน้ำ
                        และสีสันของการพักผ่อนประจำปีนี้คือปาร์ตี้ธีมย้อนยุคไปในสไตล์อโยธยา</p>
                        <div class="modal-body bg-white">
                            <div class="row">
                                @for ($i =1; $i <= 6; $i++)
                                <div class="col-md-6">
                                    <figure class="img-container">
                                        <img class="img-gallery" src="{{ asset('img/activity/Pivot-outing-2016/outing-2016-'.$i.'.jpg') }}">
                                    <figure class="img-container">
                                </div>
                                @endfor

                            </div>
                    </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-xs-12">
                <div class="card border-0">
                    <a data-toggle="modal" data-target=".bd2-example-modal-xl" href="#" class="text-primary">
                        <img class="img-activity-card" src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-1.jpg') }}">
                    </a>
                    <div class="card-body p-0">
                        <h4 class="activity-h4">Pivot Football#2 2018 @ Super Kick, Bangkok 10</h4>
                        <p class="activity-p">10 กันยายน 2561 กับ บรรยากาศการแข่งขันฟุตบอลกระชับความสัมพันธ์<br>
                            <a data-toggle="modal" data-target=".bd2-example-modal-xl" href="#" class="text-primary">เพิ่มเติม</a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="modal fade bd2-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header border-0 pb-0">
                            <h4 class="modal-title">Pivot Football#2 2018</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <p class="modal-p">10 กันยายน 2561 กับ บรรยากาศการแข่งขันฟุตบอลกระชับความสัมพันธ์</p>
                        <div class="modal-body bg-white">
                            <div class="row">
                                <div class="row">
                                @for ($i =1; $i <= 6; $i++)
                                <div class="col-md-6">
                                    <figure class="img-container">
                                        <img class="img-gallery" src="{{ asset('img/activity/Pivot-Football-2-2018/Football-2018-'.$i.'.jpg') }}">
                                    <figure class="img-container">
                                </div>
                                @endfor

                            </div>

                            </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-12 col-md-6 col-xs-12">
                <div class="card border-0">
                    <a data-toggle="modal" data-target=".bd3-example-modal-xl" href="#" class="text-primary">
                    <img class="img-activity-card" src="{{ asset('img/activity/Pivot-Outing-2018/Outing-2018-1.jpg') }}">
                    </a>
                    <div class="card-body p-0">
                        <h4 class="activity-h4">Pivot Outing 2018 colorfull party @ The pine resort Pratumthani</h4>
                        <p class="activity-p">สนุกสนานกับกิจกรรมสุดเอ็กซ์ตรีม พร้อมกับรางวัลจากเกมที่อาศัยแค่ดวงล้วนๆ<br>
                            <a data-toggle="modal" data-target=".bd3-example-modal-xl" href="#" class="text-primary">เพิ่มเติม</a>
                         </p>
                    </div>
                </div>
            </div>

            <div class="modal fade bd3-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header border-0 pb-0">
                            <h4 class="modal-title">Pivot Outing 2018</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <p class="modal-p">colorfull party @ The pine resort Pratumthaniสนุกสนานกับกิจกรรมสุดเอ็กซ์ตรีม พร้อมกับรางวัลจากเกมที่อาศัยแค่ดวงล้วนๆ</p>
                        <div class="modal-body bg-white">
                            <div class="row">
                               @for ($i =1; $i <= 6; $i++)
                                <div class="col-md-6">
                                    <figure class="img-container">
                                        <img class="img-gallery" src="{{ asset('img/activity/Pivot-outing-2018/outing-2018-'.$i.'.jpg') }}">
                                    <figure class="img-container">
                                </div>
                                @endfor

                            </div>
                    </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-md-6 col-xs-12">
                <div class="card border-0">
                    <a data-toggle="modal" data-target=".bd4-example-modal-xl" href="#" class="text-primary">
                    <img class="img-activity-card" src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-1.jpg') }}">
                    </a>
                    <div class="card-body p-0">
                        <h4 class="activity-h4">Pivot Outing 2019 Fun Run @ The Resort Suanpung</h4>
                        <p class="activity-p">สำหรับ มกราคม 2019 ที่ผ่านมานี้ ไพวอทได้ พาเพื่อนพนักงาน ไปเยือนเมืองแห่ง
                            ไออุ่นขุนเขา ที่ เดอะรีสอร์ทสวนผึ้ง จังหวัดราชบุรี มีกิจกรรม อาทิ การบริจาคสิ่ง ของให้กับเด็กๆ พักผ่อนตามอัธยาศัย เดินถ่ายรูปเก๋ๆ หรือ เล่นสวนน้ำแบบชิคๆ
                            และ ปาร์ตี้สุดเหวี่ยงกิจกรรมยามค่ำคืน ฟินกับการวิ่งขึ้นเขายามเช้า กับกิจกรรม Pivot Funrun 2019 สามารถรับชมภาพคูลๆ ได้ในอัลบั้มเท่านั้นเลยค่ะ<br>
                            <a data-toggle="modal" data-target=".bd4-example-modal-xl" href="#" class="text-primary">เพิ่มเติม</a>
                         </p>
                        </p>
                    </div>
                </div>
            </div>

            <div class="modal fade bd4-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-xl">
                    <div class="modal-content">
                        <div class="modal-header border-0 pb-0">
                            <h4 class="modal-title">Pivot Outing 2019<br>Fun Run @ The Resort Suanpung</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                            <p class="modal-p">สำหรับ มกราคม 2019 ที่ผ่านมานี้ ไพวอทได้ พาเพื่อนพนักงาน ไปเยือนเมืองแห่ง
                            ไออุ่นขุนเขา ที่ เดอะรีสอร์ทสวนผึ้ง จังหวัดราชบุรี มีกิจกรรม อาทิ การบริจาคสิ่ง ของให้กับเด็กๆ พักผ่อนตามอัธยาศัย เดินถ่ายรูปเก๋ๆ หรือ เล่นสวนน้ำแบบชิคๆ
                            และ ปาร์ตี้สุดเหวี่ยงกิจกรรมยามค่ำคืน ฟินกับการวิ่งขึ้นเขายามเช้า กับกิจกรรม Pivot Funrun 2019 สามารถรับชมภาพคูลๆ ได้ในอัลบั้มเท่านั้นเลยค่ะ</p>
                        <div class="modal-body bg-white">
                            <div class="row">
                                @for ($i =1; $i <= 6; $i++)
                                <div class="col-md-6">
                                    <figure class="img-container">
                                        <img class="img-gallery" src="{{ asset('img/activity/Pivot-outing-2019/outing-2019-'.$i.'.jpg') }}">
                                    <figure class="img-container">
                                </div>
                                @endfor

                            </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


    @endif

@endsection
