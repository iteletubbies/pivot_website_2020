const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css')
   .copy('node_modules/vesperr/icofont/icofont.min.css', 'public/css')
   .copy('node_modules/vesperr/icofont/fonts/icofont.woff', 'public/css')
   .copy('node_modules/vesperr/icofont/fonts/icofont.woff2', 'public/css')
   .copy('node_modules/vesperr/remixicon/remixicon.css', 'public/css')
   .copy('node_modules/vesperr/remixicon/remixicon.eot', 'public/css')
   .copy('node_modules/vesperr/remixicon/remixicon.woff2', 'public/css')
   .copy('node_modules/vesperr/remixicon/remixicon.woff', 'public/css')
   .copy('node_modules/vesperr/remixicon/remixicon.svg', 'public/css')
   .copy('node_modules/vesperr/boxicons/css/boxicons.min.css', 'public/css')
   .copy('node_modules/vesperr/boxicons/fonts/boxicons.woff2', 'public/css')
   .copy('node_modules/vesperr/boxicons/fonts/boxicons.woff', 'public/css')
   .copy('node_modules/vesperr/boxicons/fonts/boxicons.ttf', 'public/css')
   .copy('node_modules/vesperr/owl.carousel/assets/owl.carousel.min.css', 'public/css')
   .copy('node_modules/vesperr/venobox/venobox.css', 'public/css')
   .copy('node_modules/vesperr/jquery.easing/jquery.easing.min.js', 'public/js')
   .copy('node_modules/vesperr/waypoints/jquery.waypoints.min.js', 'public/js')
   .copy('node_modules/vesperr/counterup/counterup.min.js', 'public/js')
   .copy('node_modules/vesperr/owl.carousel/owl.carousel.min.js', 'public/js')
   .copy('node_modules/vesperr/isotope-layout/isotope.pkgd.min.js', 'public/js')
   .copy('node_modules/vesperr/venobox/venobox.min.js', 'public/js')
   .copy('node_modules/vesperr/aos/aos.js', 'public/js')
   .copy('resources/css/style.css', 'public/css')
   .copy('resources/js/main.js', 'public/js')
   .js('node_modules/popper.js/dist/popper.js', 'public/js').sourceMaps();



