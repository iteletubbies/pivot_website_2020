<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $isTablet    = $agent->isTablet();
    $frontend    = ($isDesktop == 1 || $isTablet == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1 || $isTablet == 1) ? 'isDesktop' : 'isMobile';

    return view('welcome', [
        'frontend'    => $frontend,
        'title_agent' => $title_agent,
    ]);
})->name('home');

Route::get('/service', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $device      = $agent->platform();
    $frontend    = ($isDesktop == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1) ? 'isDesktop' : 'isMobile';
    $result      = ($device == 'Macintosh' && $isDesktop != 1) ? 'isTablet' : 'None';

    return view('service', [
        'frontend'    => $frontend,
        'result'      => $device,
        'title_agent' => $title_agent,
    ]);
})->name('service');

Route::get('/about', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $isTablet    = $agent->isTablet();
    $frontend    = ($isDesktop == 1 || $isTablet == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1 || $isTablet == 1) ? 'isDesktop' : 'isMobile';

    return view('about', [
        'frontend'    => $frontend,
        'title_agent' => $title_agent,
    ]);
})->name('about');

Route::get('/career', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $isTablet    = $agent->isTablet();
    $frontend    = ($isDesktop == 1 || $isTablet == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1 || $isTablet == 1) ? 'isDesktop' : 'isMobile';

    return view('career', [
        'frontend'    => $frontend,
        'title_agent' => $title_agent,
    ]);
})->name('career');

Route::get('/activity', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $isTablet    = $agent->isTablet();
    $frontend    = ($isDesktop == 1 || $isTablet == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1 || $isTablet == 1) ? 'isDesktop' : 'isMobile';

    return view('activity', [
        'frontend'    => $frontend,
        'title_agent' => $title_agent,
    ]);
})->name('activity');

Route::get('/contact', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $isTablet    = $agent->isTablet();
    $frontend    = ($isDesktop == 1 || $isTablet == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1 || $isTablet == 1) ? 'isDesktop' : 'isMobile';

    return view('contact', [
        'frontend'    => $frontend,
        'title_agent' => $title_agent,
    ]);
})->name('contact');

Route::get('/cookies-policy', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $isTablet    = $agent->isTablet();
    $frontend    = ($isDesktop == 1 || $isTablet == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1 || $isTablet == 1) ? 'isDesktop' : 'isMobile';

    return view('cookies-policy', [
        'frontend'    => $frontend,
        'title_agent' => $title_agent,
    ]);
})->name('cookies-policy');

Route::get('/privacy-notice', function () {
    $agent       = new \Jenssegers\Agent\Agent;
    $isDesktop   = $agent->isDesktop();
    $isTablet    = $agent->isTablet();
    $frontend    = ($isDesktop == 1 || $isTablet == 1) ? 'layouts/frontend_desktop' : 'layouts/frontend';
    $title_agent = ($isDesktop == 1 || $isTablet == 1) ? 'isDesktop' : 'isMobile';

    return view('privacy-notice', [
        'frontend'    => $frontend,
        'title_agent' => $title_agent,
    ]);
})->name('privacy-notice');
